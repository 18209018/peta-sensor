<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Wilayah_model extends MY_Model {
    
    function __construct() {
        parent::__construct();
        $this->_table = 'wilayah';
    }

	function update_wilayah($data){
		$id = $data['id'];
		unset ($data['id']);
		return $this->update($id, $data);
	}
	
	function get_dropdown()
	{
		$all = $this->get_all();
		$retval = array();
		foreach ($all as $wilayah) {
			$retval[$wilayah->id] = $wilayah->nama_wilayah;
		}
		return $retval;
	}

}

/** End of file */