<?php

class Daerah_model extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table = 'daerah';
    }

    function update_daerah($data) {
        $id = $data['id'];
        unset($data['id']);
        //$this->update_all(array('is_wil_default'=>0));
        return $this->update($id, $data);
    }

    function get_all_list() {
        $this->db->select('*,d.id as id_daerah')
                ->join('wilayah w', 'w.id = d.id_wilayah')
                ->from('daerah d');
        return $this->db->get()->result();
    }
	
	function get_dropdown($id)
	{
		$all = $this->get_many_by('id_wilayah',$id);
		$retval = array();
		foreach ($all as $daerah) {
			$retval[$daerah->id] = $daerah->nama_daerah ." ".$daerah->nomor_daerah;
		}
		return $retval;
	}

}
