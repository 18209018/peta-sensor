<?php

class Sensor_data_model extends MY_Model{
    function __construct() {
        parent::__construct();
        $this->_table = 'sensor_data';
    }
    
    function update_data_sensor($data){
		$id = $data['id'];
		unset ($data['id']);
		return $this->update($id, $data);
	}
}
