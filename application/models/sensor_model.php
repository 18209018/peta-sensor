<?php

class Sensor_model extends MY_Model{
    function __construct() {
        parent::__construct();
        $this->_table = 'sensor';
    }
    
	function get_all_list($id=-1) {
		if($id!=-1)
			$this->db->where('s.id',$id);
        $this->db->select('*,d.id as id_daerah_asli, w.id as id_wilayah_asli, s.id as id_sensor_asli')
				->join('daerah d', 's.id_daerah = d.id')
                ->join('wilayah w', ' d.id_wilayah = w.id')
                ->from('sensor s');
		
        if($id!=-1)
			return $this->db->get()->row();
		return $this->db->get()->result();
    }
	
	function update_sensor($data){
		$id = $data['id'];
		unset ($data['id']);
		return $this->update($id, $data);
	}
	
    function get_sensor_in_wilayah($id_wilayah){
        $this->db->join('daerah','daerah.id=sensor.id_daerah');
        $this->db->join('wilayah','wilayah.id=daerah.id_wilayah');
        $result = $this->db->get_where('sensor',array('id_wilayah'=>$id_wilayah));
        return $result->result_array();
    }
    
    function get_sensor_data_last_day($id_sensor){
        $this->db->join('sensor_data','sensor.id=sensor_data.id_sensor');
        $this->db->order_by('timestamp','ASC');
        $this->db->select('*');
        $this->db->select('TIME(timestamp) as time');
        $result = $this->db->get_where('sensor',array('id_sensor'=>$id_sensor,'DATE(timestamp)'=>date('Y-m-d')));
        return $result->result_array();
    }
    
    function get_sensor_data_last_day_perhour($id_sensor){
        $this->db->join('sensor_data','sensor.id=sensor_data.id_sensor');
        $this->db->order_by('timestamp','ASC');
        $this->db->select('*');
        $this->db->select('TIME(timestamp) as time');
        $result = $this->db->get_where('sensor',array('id_sensor'=>$id_sensor,'MINUTE(timestamp)'=>0,'SECOND(timestamp)'=>0,'DATE(timestamp)'=>date('Y-m-d')));
        return $result->result_array();
    }
    
    function get_sensor_data_in_wilayah($id_wilayah){
//        $sql = "SELECT * from "
//                . "(SELECT id_sensor, MAX(timestamp) as max, SUBTIME(MAX(timestamp),'00:05:00') as min from sensor_data GROUP BY id_sensor) as gab "
//                . "INNER JOIN sensor ON gab.id_sensor=sensor.id "
//                . "INNER JOIN daerah ON daerah.id=sensor.id_daerah "
//                . "INNER JOIN wilayah ON wilayah.id=daerah.id_wilayah "
//                . "WHERE wilayah.id=".$id_wilayah;
//        $maxmintable = $this->db->query($sql)->result_array();
//        $avg = array();
//        foreach($maxmintable as $m){
//            $sql = "SELECT AVG(O3) as O3,AVG(CO2) as CO2,AVG(SO2) as SO2,AVG(NO2) as NO2,AVG(temperatur) as T from sensor_data "
//                    . "WHERE timestamp>='".$m['min']."' "
//                    . "AND timestamp<='".$m['max']."' "
//                    . "AND id_sensor=".$m['id_sensor']." "
//                    . "GROUP BY id_sensor";
//            $avg[$m['id_sensor']] = $this->db->query($sql)->result_array();
//        }
//        return $avg;
        $sql = 'SELECT * FROM daerah INNER JOIN sensor ON sensor.id_daerah=daerah.id INNER JOIN (SELECT * FROM sensor_data INNER JOIN (SELECT id_sensor as idsensor, MAX(timestamp) as max FROM sensor_data GROUP BY id_sensor) as max_tab WHERE max_tab.idsensor=sensor_data.id_sensor AND sensor_data.timestamp=max_tab.max) as val ON sensor.id=val.id_sensor WHERE id_wilayah='.$id_wilayah;
        return $this->db->query($sql)->result_array();
    }
	
	function get_dropdown()
	{
		$all = $this->get_all();
		$retval = array();
		foreach ($all as $sensor) {
			$retval[$sensor->id] = "Sensor".$sensor->id;
		}
		return $retval;
	}
	
}
