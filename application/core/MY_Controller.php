<?php
class MY_Controller extends CI_Controller{
    function __construct() {
        parent::__construct();
    }
}

class Auth_controller extends MY_Controller{
    function __construct() {
        parent::__construct();
        if (!$this->ion_auth->logged_in()) {
            //redirect them to the login page
            redirect('auth/login', 'refresh');
        }
    }
}