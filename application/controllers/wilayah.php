<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Wilayah extends Auth_controller {

    function __construct() {
        parent::__construct();
        $this->load->model("wilayah_model");
    }

    function index() {
        $this->wilayah_list();
    }

    function wilayah_list() {
        //array of user object
        $data['page_heading'] = 'Wilayah management';
        $data['wilayah'] = $this->wilayah_model->get_all();
        $this->template->display('wilayah/wilayah_list', $data);
    }

    function add() {
        $this->load->library('form_validation');

        $data['page_heading'] = 'Wilayah management';

        //validate form input
        $this->form_validation->set_rules('wilayah', 'wilayah', 'required|xss_clean');
		$insert_data = array();
        if ($this->form_validation->run() === TRUE) {

            $insert_data['nama_wilayah'] = $this->input->post('wilayah');
			$insert_data['is_default'] = $this->input->post('is_default')=='default'?1:0;
			if($insert_data['is_default']==1)$this->wilayah_model->update_all(array('is_default'=>0));
        }

        if ($this->form_validation->run() == true && $this->wilayah_model->insert($insert_data, FALSE)) {
            //check to see if we are creating the user
            //redirect them back to the admin page
            $this->session->set_flashdata('message', "wilayah berhasil ditambahkan");
            redirect("wilayah");
        }

		$data['wilayah'] = array(
            'name' => 'wilayah',
            'id' => 'wilayah',
            'type' => 'text',
            'class' => 'form-control'
        );
		
		$data['is_default'] = array(
            'name' => 'is_default',
            'id' => 'is_default',
            'value'       => 'default',
			'checked'     => FALSE,
            'class' => 'form-control'
        );
		
        $this->template->display('wilayah/add', $data);
    }

    function edit($id) {
        $this->load->library('form_validation');

        $data['page_heading'] = 'Wilayah management';

        $wilayah = $this->wilayah_model->get($id);

        //validate form input
        $this->form_validation->set_rules('nama_wilayah', 'nama_wilayah', 'required|xss_clean');

        if (isset($_POST) && !empty($_POST)) {

            $upd_data = array(
                'nama_wilayah' => $this->input->post('nama_wilayah'),
				'is_default' => $this->input->post('is_default')=='default'?1:0,
				'id'=>$id
            );
			if($upd_data['is_default']==1)$this->wilayah_model->update_all(array('is_default'=>0));
            if ($this->form_validation->run() === TRUE) {
                $this->wilayah_model->update_wilayah($upd_data);
				
                $this->session->set_flashdata('message', "Edit wilayah success");

                redirect(base_url() . 'wilayah');
            }
        }


        $data['nama_wilayah'] = array(
            'name' => 'nama_wilayah',
            'id' => 'nama_wilayah',
			'value' => $this->form_validation->set_value('nama_wilayah', $wilayah->nama_wilayah),
            'type' => 'text',
            'class' => 'form-control'
        );
		
		$data['is_default'] = array(
            'name' => 'is_default',
            'id' => 'is_default',
			'value'       => 'default',
			'checked'     => $wilayah->is_default==1,
            'class' => 'form-control'
        );

        $data['id'] = $id;

        $this->template->display('wilayah/edit', $data);
    }

    function view($id) {
        $data['page_heading'] = 'Wilayah management';

        $data['wilayah'] = $this->wilayah_model->get($id);

        $this->template->display('wilayah/view', $data);
    }

    function delete($id) {
        $this->wilayah_model->delete($id);
        $this->session->set_flashdata('message', "wilayah sudah dihapus");
        redirect(base_url() . 'wilayah');
    }

	function do_upload($id)
	{
		$config['upload_path'] = './assets/uploads/';
		$config['allowed_types'] = 'xls|xlsx';
		$config['overwrite'] = true;
		$file = $_FILES['userfile']['name'];
		$ext = substr(strrchr($file, '.'), 1); 
		$config['file_name'] = 'temp.'.$ext;
		$this->load->library('upload', $config);
		
		if ( ! $this->upload->do_upload())
		{
			$this->session->set_flashdata('message', $this->upload->display_errors());
			
			//redirect(base_url() . 'wilayah');
		}
		else
		{
			//$this->session->set_flashdata('message', "masukan excel sensor success");
			$id==null?$this->excelReadWilayah($config['upload_path'].$config['file_name']):$this->excelReadDaerah($config['upload_path'].$config['file_name'],$id);
			//redirect(base_url() . 'wilayah');
		}
	}
	
	function excelReadWilayah($inputFilePath)
	{
		
		$this->load->library('excel');

		//$inputFileName = './sampleData/example1.xls';

		//  Read your Excel workbook
		try {
			
			$inputFileType = PHPExcel_IOFactory::identify($inputFilePath);
			$objReader = PHPExcel_IOFactory::createReader($inputFileType);
			$objPHPExcel = $objReader->load($inputFilePath);
		} catch(Exception $e) {
			die('Error loading file "'.pathinfo($inputFilePath,PATHINFO_BASENAME).'": '.$e->getMessage());
		}

		//  Get worksheet dimensions
		//$sheet = $objPHPExcel->getSheet(0); 
		$sheetData = $objPHPExcel->getSheet(0)->toArray(null,true,true,true);
		//$highestRow = $sheet->getHighestRow(); 
		//$highestColumn = $sheet->getHighestColumn();
		foreach ($sheetData as $data) {
			$insert_data['nama_wilayah'] = $data['A'];

			try
			{
				$this->wilayah_model->insert($insert_data, FALSE);
			}
			catch(Exception $e) {
				die($e->getMessage());
			}
		}
		$this->session->set_flashdata('message', "wilayah berhasil ditambahkan");
		redirect("wilayah");
	}
	
	function WriteWilayah($mode)
	{
		$this->load->library('excel');
		$this->load->library('excelwriter');
		PHPExcel_Settings::setPdfRenderer(PHPExcel_Settings::PDF_RENDERER_DOMPDF, './application/third_party/pdf');
		$objPHPExcel = new PHPExcel;
		// set default font
		$objPHPExcel->getDefaultStyle()->getFont()->setName('Calibri');
		// set default font size
		$objPHPExcel->getDefaultStyle()->getFont()->setSize(8);
		// create the writer
		if($mode==excel)
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
		else
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "PDF");
		// writer already created the first sheet for us, let's get it
		$objSheet = $objPHPExcel->getActiveSheet();
		// rename the sheet
		$objSheet->setTitle('Wilayah');
		// let's bold and size the header font and write the header
		// as you can see, we can specify a range of cells, like here: cells from A1 to A4
		$objSheet->getStyle('A1')->getFont()->setBold(true)->setSize(12);
		// write header
		$objSheet->getCell('A1')->setValue('Nama Wilayah');
		$wilayah = $this->wilayah_model->get_all();
		for($i=0;$i<count($wilayah);$i++)
		{
			$objSheet->getCell('A'.($i+2))->setValue($wilayah[$i]->nama_wilayah);
			$objSheet->getStyle('A'.($i+2))->getBorders()->getOutline()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
		}
		// create medium border around the table
		$objSheet->getStyle('A1:A'.(count($wilayah)+2))->getBorders()->getOutline()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);
		// autosize the columns
		$objSheet->getColumnDimension('A')->setAutoSize(true);
		// write the file
		if($mode==excel)
		{
			$objWriter->save('./assets/uploads/Wilayah.xlsx');
			redirect("./assets/uploads/Wilayah.xlsx");
		}
		else
		{
			$objWriter->save('./assets/uploads/Wilayah.pdf');
			redirect("./assets/uploads/Wilayah.pdf");
		}
	}
	
	function WriteDaerah($mode,$id)
	{
		$this->load->library('excel');
		$this->load->library('excelwriter');
		$this->load->model("daerah_model");
		PHPExcel_Settings::setPdfRenderer(PHPExcel_Settings::PDF_RENDERER_DOMPDF, './application/third_party/pdf');
		$objPHPExcel = new PHPExcel;
		// set default font
		$objPHPExcel->getDefaultStyle()->getFont()->setName('Calibri');
		// set default font size
		$objPHPExcel->getDefaultStyle()->getFont()->setSize(8);
		// create the writer
		if($mode==excel)
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
		else
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "PDF");
		// writer already created the first sheet for us, let's get it
		$objSheet = $objPHPExcel->getActiveSheet();
		// rename the sheet
		$objSheet->setTitle('Daerah');
		// let's bold and size the header font and write the header
		// as you can see, we can specify a range of cells, like here: cells from A1 to A4
		$objSheet->getStyle('A1:B1')->getFont()->setBold(true)->setSize(12);
		// write header
		$objSheet->getCell('A1')->setValue('Nama Daerah');
		$objSheet->getCell('B1')->setValue('Nomor Daerah');
		$daerah = $this->daerah_model->get_many_by('id_wilayah',$id);
		for($i=0;$i<count($daerah);$i++)
		{
			$objSheet->getCell('A'.($i+2))->setValue($daerah[$i]->nama_daerah);
			$objSheet->getStyle('A'.($i+2))->getBorders()->getOutline()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
			$objSheet->getCell('B'.($i+2))->setValue($daerah[$i]->nomor_daerah);
			$objSheet->getStyle('B'.($i+2))->getBorders()->getOutline()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
		}
		// create medium border around the table
		$objSheet->getStyle('A1:B'.(count($daerah)+2))->getBorders()->getOutline()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);
		// autosize the columns
		$objSheet->getColumnDimension('A')->setAutoSize(true);
		$objSheet->getColumnDimension('B')->setAutoSize(true);
		// write the file
		if($mode==excel)
		{
			$objWriter->save('./assets/uploads/Daerah.xlsx');
			redirect("./assets/uploads/Daerah.xlsx");
		}
		else
		{
			$objWriter->save('./assets/uploads/Daerah.pdf');
			redirect("./assets/uploads/Daerah.pdf");
		}
	}
	
	function excelReadDaerah($inputFilePath,$id)
	{
		$this->load->model("daerah_model");
		$this->load->library('excel');

		//$inputFileName = './sampleData/example1.xls';

		//  Read your Excel workbook
		try {
			
			$inputFileType = PHPExcel_IOFactory::identify($inputFilePath);
			$objReader = PHPExcel_IOFactory::createReader($inputFileType);
			$objPHPExcel = $objReader->load($inputFilePath);
		} catch(Exception $e) {
			die('Error loading file "'.pathinfo($inputFilePath,PATHINFO_BASENAME).'": '.$e->getMessage());
		}

		//  Get worksheet dimensions
		//$sheet = $objPHPExcel->getSheet(0); 
		$sheetData = $objPHPExcel->getSheet(0)->toArray(null,true,true,true);
		//$highestRow = $sheet->getHighestRow(); 
		//$highestColumn = $sheet->getHighestColumn();
		foreach ($sheetData as $data) {
			$insert_data['nama_daerah'] = $data['A'];
			$insert_data['nomor_daerah'] = $data['B'];
			$insert_data['id_wilayah'] = $id;
			try
			{
				$this->daerah_model->insert($insert_data, FALSE);
			}
			catch(Exception $e) {
				die($e->getMessage());
			}
		}
		$this->session->set_flashdata('message', "daerah berhasil ditambahkan");
		redirect("daerah");
	}
	
}
