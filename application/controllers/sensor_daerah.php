<?php
class Sensor_daerah extends CI_Controller{
    
    function __construct() {
        parent::__construct();
    }
    
    function map_daerah($id_daerah){
        $this->load->model('daerah_model','daerah');
        $this->load->model('sensor_model','sen');
        $this->load->library('googlemaps');
        $this->config->load('googlemaps');
        
        $daerah = $this->daerah->as_array()->get($id_daerah);
        $sensor = $this->sen->as_array()->get_many_by('id_daerah',$id_daerah);
        
        
        $gmap_conf = $this->config->item('gmap');   //default config buat api key
        
        $geocode = $this->googlemaps->get_lat_long_from_address($daerah['nama_daerah']);
        $lat = $geocode[0];
        $lng = $geocode[1];
        $gmap_conf['center']=$lat.', '.$lng;
        $gmap_conf['zoom']=15;
        
        $data['sensor']=array();
        $i=0;
        foreach($sensor as $s){
            $marker = array();
            $marker['position'] = $s['lat'].', '.$s['lang'];
            $marker['onclick'] = 'popup_grafik('.$s['id'].');';
            $this->googlemaps->add_marker($marker);
            
            $data['sensor'][$i++]=array('id'=>$s['id'],'lat'=>$s['lat'],'lang'=>$s['lang']);
        }
        
        //buat petanya
        $this->googlemaps->initialize($gmap_conf);
        
        $data['daerah']=$daerah;
        $data['map'] = $this->googlemaps->create_map();
        $data['add_script'] = $this->load->view('sensor_daerah/map_daerah_add_script',$data,true);
        $this->template->display_sidebarless('sensor_daerah/map_daerah',$data);
    }
    
    function grafik_sensor($id_sensor){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $this->load->library('ci_date');
        $this->load->model('sensor_model','sen');
        
        $data_sensor = $this->sen->get_sensor_data_last_day($id_sensor);
        $data['o3']=array();
        $data['co2']=array();
        $data['so2']=array();
        $data['no2']=array();
        $data['temp']=array();
        $i=0;
        foreach($data_sensor as $d){
            $date = date_create_from_format('H:i:s', $d['time']);
            $formatted = date_format($date, 'H:i');
            $data['o3']['val'][]=$d['O3'];
            $data['co2']['val'][]=$d['CO2'];
            $data['no2']['val'][]=$d['NO2'];
            $data['so2']['val'][]=$d['SO2'];
            if($i%12==0){
                $data['o3']['label'][]=$formatted;
                $data['co2']['label'][]=$formatted;
                $data['no2']['label'][]=$formatted;
                $data['so2']['label'][]=$formatted;
            }else{
                $data['o3']['label'][]='';
                $data['co2']['label'][]='';
                $data['no2']['label'][]='';
                $data['so2']['label'][]='';
            }
            $i++;
        }
        echo $this->load->view('sensor_daerah/modal_grafik_sensor',$data,true);
    }
    
    function tabel_sensor($id_sensor){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $this->load->library('ci_date');
        $this->load->model('sensor_model','sen');
        
        $data_sensor = $this->sen->get_sensor_data_last_day_perhour($id_sensor);
        $data['data_sensor'] = array_reverse($data_sensor);
        
        echo $this->load->view('sensor_daerah/modal_tabel_sensor',$data,true);
    }
    
    
    function seeding_data_sensor(){
        $this->load->model('sensor_model','sen');
        $this->load->model('sensor_data_model','sendata');
        
        $sensor = $this->sen->as_array()->get_all();
        $this->sendata->truncate();
        
        $ins_data = array();
        
        $i=0;
        $date=null;
        foreach($sensor as $sen){
            $cur_date = date_create_from_format('Y-m-d H:i:s', date('Y-m-d').' 00:00:00');
            for($j=0;$j<24;$j++){
                $this->fire_php->log('Masuk');
                $ins_data[$i]['id_sensor']=$sen['id'];
                $ins_data[$i]['O3']=rand(23, 25);
                $ins_data[$i]['CO2']=rand(23, 25);
                $ins_data[$i]['SO2']=rand(23, 25);
                $ins_data[$i]['NO2']=rand(23, 25);
                $ins_data[$i]['temperatur']=rand(25, 35);
                $ins_data[$i]['timestamp']=  date_format($cur_date,'Y-m-d H:i:s');
                $intv = date_interval_create_from_date_string('5 minutes');
                $cur_date = date_add($cur_date, $intv);
                $i++;
            }
        }
        $this->fire_php->log($ins_data);
        $this->sendata->insert_many($ins_data);
    }
    
}
