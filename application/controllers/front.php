<?php

class Front extends CI_Controller{
    
    function __construct() {
        parent::__construct();
    }
    
    function index(){
        $this->select_daerah();
    }
    
    function select_daerah(){
        $this->load->model('wilayah_model','wil');
        $this->load->library('googlemaps');
        $this->config->load('googlemaps');
        
        //siapin config buat petanya
        $gmap_conf = $this->config->item('gmap');   //default config buat api key
        
        //ambil wilayah default
        $wilayah = $this->wil->as_array()->get_all();
        
        foreach($wilayah as $w){
            $geocode = $this->googlemaps->get_lat_long_from_address($w['nama_wilayah']); 
            $lat = $geocode[0];
            $lng = $geocode[1];
            $w['str_lat_lang']=$lat.', '.$lng;
            if($w['is_default']){
                $gmap_conf['center']=$lat.', '.$lng;
                $data['selected_wilayah']=$lat.', '.$lng;
            }
            //buat markernya
            $marker = array();
            $marker['position'] = $lat.', '.$lng;
            $marker['infowindow_content']=$w['nama_wilayah'];
            $this->googlemaps->add_marker($marker);
            $data['wilayah'][$w['str_lat_lang']]=$w['nama_wilayah'];
            $data['navigate'][$w['str_lat_lang']]=$w['id'];
        }
        
        
        //buat petanya
        $this->googlemaps->initialize($gmap_conf);
        
        //masukin ke variabel buat dikirim ke view
        $data['map'] = $this->googlemaps->create_map();
        $data['add_script'] = $this->load->view('front/select_daerah_add_script',$data,true);
        $this->template->display_sidebarless('front/select_daerah',$data);
    }
    
}
