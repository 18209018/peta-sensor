<?php

class Sensor_wilayah extends CI_Controller {
    
    function __construct() {
        parent::__construct();
    }
    
    function index(){
        
    }
    
    function map_wilayah($id_wilayah){
        $this->load->model('wilayah_model','wil');
        $this->load->model('daerah_model','daerah');
        $this->load->model('sensor_model','sen');
        $this->load->library('googlemaps');
        $this->config->load('googlemaps');
        
        //ambil wilayah
        $w = $this->wil->as_array()->get($id_wilayah);
        
        //siapin config buat petanya
        $gmap_conf = $this->config->item('gmap');   //default config buat api key
        //buat heatmap butuh geometry ama visualization
        $gmap_conf['geometry']=TRUE;
        $gmap_conf['visualization']=TRUE;
        
        $geocode = $this->googlemaps->get_lat_long_from_address($w['nama_wilayah']); 
        $lat = $geocode[0];
        $lng = $geocode[1];
        $gmap_conf['center']=$lat.', '.$lng;
        
        //ambil sensor di wilayah tsb
//        $all_sensor = $this->sen->get_sensor_in_wilayah($id_wilayah);
        
        $avg = $this->sen->get_sensor_data_in_wilayah($id_wilayah);
        //buat petanya
        $this->googlemaps->initialize($gmap_conf);
        //masukin ke variabel buat dikirim ke view
//        $data['all_sensor']=  json_encode($all_sensor,JSON_NUMERIC_CHECK);
        $data['map'] = $this->googlemaps->create_map();
        $data['avg'] = json_encode($avg,JSON_NUMERIC_CHECK);
        $data['daerah']=  $this->daerah->as_array()->get_many_by('id_wilayah',$id_wilayah);
        $data['add_script'] = $this->load->view('sensor_wilayah/map_wilayah_add_script',$data,true);
        $this->template->display_sidebarless('sensor_wilayah/map_wilayah',$data);
    }
}
