<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User extends Auth_controller {

    function __construct() {
        parent::__construct();
    }

    function index() {
        $this->user_list();
    }

    function user_list() {
        //array of user object
        $data['page_heading'] = 'User management';
        $data['users'] = $this->ion_auth->users()->result();
        $this->template->display('user/user_list', $data);
    }

    function add() {
        $this->load->library('form_validation');

        $data['page_heading'] = 'User management';

        //validate form input
        $this->form_validation->set_rules('username', 'username', 'required|xss_clean');
        $this->form_validation->set_rules('first_name', 'first Name', 'required|xss_clean');
        $this->form_validation->set_rules('last_name', 'last Name', 'required|xss_clean');
        $this->form_validation->set_rules('email', 'email', 'required|valid_email|is_unique[users.email]');
        $this->form_validation->set_rules('password', 'password', 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[password_confirm]');
        $this->form_validation->set_rules('password_confirm', 'passowrd confirm', 'required');

        if ($this->form_validation->run() === TRUE) {

            $username = $this->input->post('username');
            $email = strtolower($this->input->post('email'));
            $password = $this->input->post('password');

            $additional_data = array(
                'first_name' => $this->input->post('first_name'),
                'last_name' => $this->input->post('last_name')
            );
        }

        if ($this->form_validation->run() == true && $this->ion_auth->register($username, $password, $email, $additional_data,1)) {
            //check to see if we are creating the user
            //redirect them back to the admin page
            $this->session->set_flashdata('message', $this->ion_auth->messages());
            redirect("user");
        }
        $data['username'] = array(
            'name' => 'username',
            'id' => 'username',
            'type' => 'text',
            'value' => $this->form_validation->set_value('first_name'),
            'class' => 'form-control'
        );

        $data['first_name'] = array(
            'name' => 'first_name',
            'id' => 'first_name',
            'type' => 'text',
            'value' => $this->form_validation->set_value('first_name'),
            'class' => 'form-control'
        );
        $data['last_name'] = array(
            'name' => 'last_name',
            'id' => 'last_name',
            'type' => 'text',
            'value' => $this->form_validation->set_value('last_name'),
            'class' => 'form-control'
        );
        $data['email'] = array(
            'name' => 'email',
            'id' => 'email',
            'type' => 'text',
            'value' => $this->form_validation->set_value('email'),
            'class' => 'form-control'
        );
        $data['password'] = array(
            'name' => 'password',
            'id' => 'password',
            'type' => 'password',
            'class' => 'form-control'
        );
        $data['password_confirm'] = array(
            'name' => 'password_confirm',
            'id' => 'password_confirm',
            'type' => 'password',
            'class' => 'form-control'
        );

        $this->template->display('user/add', $data);
    }

    function edit($id) {
        $this->load->library('form_validation');

        $data['page_heading'] = 'User management';

        $user = $this->ion_auth->user($id)->row();

        //validate form input
        $this->form_validation->set_rules('first_name', 'first name', 'required|xss_clean');
        $this->form_validation->set_rules('last_name', 'last name', 'required|xss_clean');

        if (isset($_POST) && !empty($_POST)) {

            $upd_data = array(
                'first_name' => $this->input->post('first_name'),
                'last_name' => $this->input->post('last_name')
            );

            //update the password if it was posted
            if ($this->input->post('password')) {
                $this->form_validation->set_rules('password', 'password', 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[password_confirm]');
                $this->form_validation->set_rules('password_confirm', 'password confirm', 'required');

                $upd_data['password'] = $this->input->post('password');
            }

            if ($this->form_validation->run() === TRUE) {
                $this->ion_auth->update($user->id, $upd_data);

                $this->session->set_flashdata('message', "Edit user success");

                redirect(base_url() . 'user');
            }
        }


        $data['first_name'] = array(
            'name' => 'first_name',
            'id' => 'first_name',
            'type' => 'text',
            'value' => $this->form_validation->set_value('first_name', $user->first_name),
            'class' => 'form-control'
        );
        $data['last_name'] = array(
            'name' => 'last_name',
            'id' => 'last_name',
            'type' => 'text',
            'value' => $this->form_validation->set_value('last_name', $user->last_name),
            'class' => 'form-control'
        );
        $data['password'] = array(
            'name' => 'password',
            'id' => 'password',
            'type' => 'password',
            'class' => 'form-control'
        );
        $data['password_confirm'] = array(
            'name' => 'password_confirm',
            'id' => 'password_confirm',
            'type' => 'password',
            'class' => 'form-control'
        );

        $data['user'] = $user;

        $this->template->display('user/edit', $data);
    }

    function view($id) {
        $data['page_heading'] = 'User management';
        
        $data['user'] = $this->ion_auth->user($id)->row();
        
        $this->template->display('user/view', $data);
    }

    function delete($id) {
        $this->ion_auth->delete_user($id);
        $this->session->set_flashdata('message', $this->ion_auth->messages());
        redirect(base_url() . 'user');
    }

}
