<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Sensor extends Auth_controller {

    function __construct() {
        parent::__construct();
        $this->load->model("sensor_model");
		$this->load->model("daerah_model");
		$this->load->model("wilayah_model");
    }

    function index() {
        $this->sensor_list();
    }

    function sensor_list() {
        //array of user object
        $data['page_heading'] = 'Sensor management';
        $data['sensor'] = $this->sensor_model->get_all_list();
		//$data['wilayah'] = $this->wilayah_model->get_all();
        $this->template->display('sensor/sensor_list', $data);
    }

    function add() {
		$this->load->library('googlemaps');
        $this->config->load('googlemaps');
        $this->load->library('form_validation');

		//siapin config buat petanya
        $gmap_conf = $this->config->item('gmap');   //default config buat api key
		
        $data['page_heading'] = 'Sensor management';

        //validate form input
        $this->form_validation->set_rules('id_daerah', 'id_daerah', 'required|xss_clean');
		$this->form_validation->set_rules('lat', 'lat', 'required|xss_clean');
		$this->form_validation->set_rules('lang', 'lang', 'required|xss_clean');
		$insert_data = array();
        if ($this->form_validation->run() === TRUE) {
            $insert_data['id_daerah'] = $this->input->post('id_daerah');
			$insert_data['lat'] = $this->input->post('lat');
			$insert_data['lang'] = $this->input->post('lang');
		}
		
        if ($this->form_validation->run() == true && $this->sensor_model->insert($insert_data, FALSE)) {
            //check to see if we are creating the user
            //redirect them back to the admin page
            $this->session->set_flashdata('message', "sensor berhasil ditambahkan");
            redirect("sensor");
        }
		
		$data['lat'] = array(
            'name' => 'lat',
            'id' => 'lat',
            'type' => 'text',
            'class' => 'form-control'
        );
		
		$data['lang'] = array(
            'name' => 'lang',
            'id' => 'lang',
            'type' => 'text',
            'class' => 'form-control'
        );
		
		
		$data['id_wilayah'] = $this->wilayah_model->get_dropdown();
		$data['id_wilayah_opt'] = 'class="form-control" id="id_wilayah"';
		
		
		foreach ($data['id_wilayah'] as $key=>$value) {
			$data['id_daerah'] = $this->daerah_model->get_dropdown($key);
			$data['id_daerah_opt'] = 'class="form-control" id="id_daerah"';
			
			$geocode = $this->googlemaps->get_lat_long_from_address($value);
			$lat = $geocode[0];
			$lng = $geocode[1];
			$gmap_conf['center']=$lat.', '.$lng;
			$gmap_conf['zoom']=15;
			break;
		}
		
		
		//buat petanya
        $this->googlemaps->initialize($gmap_conf);
        //masukin ke variabel buat dikirim ke view
		$this->googlemaps->onclick = "	var latitude = event.latLng.lat();
										var longitude = event.latLng.lng();
										$('#lat').val(latitude);
										$('#lang').val(longitude);
										for (var i = 0; i < markers_map.length; i++) {
											markers_map[i].setMap(null);
										}
										var myLatlng = new google.maps.LatLng(latitude,longitude);
										var marker = new google.maps.Marker({
											position: myLatlng,
											map: map
										});
										markers_map.push(marker);
										";
        $data['map'] = $this->googlemaps->create_map();
        $data['add_script'] = $this->load->view('sensor/add_script',$data,true);
		
        $this->template->display('sensor/add', $data);
    }
	
	function list_daerah($id)
	{
		$this->load->library('googlemaps');
        $this->config->load('googlemaps');
		$retval = $this->daerah_model->get_many_by('id_wilayah',$id);
		
		//if(count($retval)>0)
		//{
			$wilayah = $this->wilayah_model->get($id);
			//echo $nama_wilayah;
			$retval[count($retval)] = $this->googlemaps->get_lat_long_from_address($wilayah->nama_wilayah);
		//}
		echo json_encode($retval);
	}

    function edit($id) {
        $this->load->library('form_validation');
		$this->load->library('googlemaps');
        $this->config->load('googlemaps');
		
		//siapin config buat petanya
        $gmap_conf = $this->config->item('gmap');   //default config buat api key
		
        $data['page_heading'] = 'Sensor management';

        $sensor = $this->sensor_model->get_all_list($id);

        //validate form input
        $this->form_validation->set_rules('id_daerah', 'id_daerah', 'required|xss_clean');
		$this->form_validation->set_rules('lat', 'lat', 'required|xss_clean');
		$this->form_validation->set_rules('lang', 'lang', 'required|xss_clean');

        if (isset($_POST) && !empty($_POST)) {

            $upd_data = array();
			$upd_data['id_daerah'] = $this->input->post('id_daerah');
			$upd_data['lat'] = $this->input->post('lat');
			$upd_data['lang'] = $this->input->post('lang');
			if ($this->form_validation->run() === TRUE) {
                $this->sensor_model->update_sensor($upd_data);

                $this->session->set_flashdata('message', "Edit sensor success");

                redirect(base_url() . 'sensor');
            }
        }

		$data['id_wilayah'] = $this->wilayah_model->get_dropdown();
		$data['id_wilayah_opt'] = 'class="form-control"';
		$data['id_wilayah_selected'] = $sensor->id_wilayah;
		
		$data['id_daerah'] = $this->daerah_model->get_dropdown($sensor->id_wilayah);
		$data['id_daerah_opt'] = 'class="form-control" id="id_daerah"';
		$data['id_daerah_selected'] = $sensor->id_daerah;

		$geocode = $this->googlemaps->get_lat_long_from_address($data['id_wilayah'][$sensor->id_wilayah]);
		$lat = $geocode[0];
		$lng = $geocode[1];
		//$gmap_conf['center']=$lat.', '.$lng;
		$gmap_conf['zoom']=15;
		
		
		$data['lat'] = array(
            'name' => 'lat',
            'id' => 'lat',
			'value' => $this->form_validation->set_value('lat', $sensor->lat),
            'type' => 'text',
            'class' => 'form-control'
        );
		
		$data['lang'] = array(
            'name' => 'lang',
            'id' => 'lang',
			'value' => $this->form_validation->set_value('lang', $sensor->lang),
            'type' => 'text',
            'class' => 'form-control'
        );
		

        $data['id'] = $id;

		$marker = array();
		$marker['position'] = $sensor->lat.', '.$sensor->lang;
		$this->googlemaps->add_marker($marker);
		
		$gmap_conf['center']=$sensor->lat.', '.$sensor->lang;
		
		//buat petanya
        $this->googlemaps->initialize($gmap_conf);
        //masukin ke variabel buat dikirim ke view
		$this->googlemaps->onclick = "	var latitude = event.latLng.lat();
										var longitude = event.latLng.lng();
										$('#lat').val(latitude);
										$('#lang').val(longitude);
										for (var i = 0; i < markers_map.length; i++) {
											markers_map[i].setMap(null);
										}
										var myLatlng = new google.maps.LatLng(latitude,longitude);
										var marker = new google.maps.Marker({
											position: myLatlng,
											map: map
										});
										markers_map.push(marker);
										";
        $data['map'] = $this->googlemaps->create_map();
		
        $this->template->display('sensor/edit', $data);
    }

    function view($id) {
		$this->load->library('googlemaps');
        $this->config->load('googlemaps');
        $data['page_heading'] = 'Sensor management';
		//siapin config buat petanya
        $gmap_conf = $this->config->item('gmap');   //default config buat api key
		$sensor = $this->sensor_model->get($id);
		$marker = array();
		$marker['position'] = $sensor->lat.', '.$sensor->lang;
		$this->googlemaps->add_marker($marker);
		$gmap_conf['center']=$sensor->lat.', '.$sensor->lang;
		$gmap_conf['zoom']=15;
		//buat petanya
        $this->googlemaps->initialize($gmap_conf);
		$data['map'] = $this->googlemaps->create_map();
        $this->template->display('sensor/view', $data);
    }

    function delete($id) {
        $this->sensor_model->delete($id);
        $this->session->set_flashdata('message', "sensor sudah dihapus");
        redirect(base_url() . 'sensor');
    }
	
	function do_upload($id)
	{
		$config['upload_path'] = './assets/uploads/';
		$config['allowed_types'] = 'xls|xlsx';
		$config['overwrite'] = true;
		$file = $_FILES['userfile']['name'];
		$ext = substr(strrchr($file, '.'), 1); 
		$config['file_name'] = 'temp.'.$ext;
		$this->load->library('upload', $config);
		
		if ( ! $this->upload->do_upload())
		{
			$this->session->set_flashdata('message', $this->upload->display_errors());
			
			//redirect(base_url() . 'wilayah');
		}
		else
		{
			//$this->session->set_flashdata('message', "masukan excel sensor success");
			$this->excelReadSensor($config['upload_path'].$config['file_name'],$id);
			//redirect(base_url() . 'wilayah');
		}
	}
	
	function excelReadSensor($inputFilePath,$id)
	{
		$this->load->model("sensor_data_model");
		$this->load->library('excel');

		//$inputFileName = './sampleData/example1.xls';

		//  Read your Excel workbook
		try {
			
			$inputFileType = PHPExcel_IOFactory::identify($inputFilePath);
			$objReader = PHPExcel_IOFactory::createReader($inputFileType);
			$objPHPExcel = $objReader->load($inputFilePath);
		} catch(Exception $e) {
			die('Error loading file "'.pathinfo($inputFilePath,PATHINFO_BASENAME).'": '.$e->getMessage());
		}

		//  Get worksheet dimensions
		//$sheet = $objPHPExcel->getSheet(0); 
		$sheetData = $objPHPExcel->getSheet(0)->toArray(null,true,true,true);
		//$highestRow = $sheet->getHighestRow(); 
		//$highestColumn = $sheet->getHighestColumn();
		foreach ($sheetData as $data) {
			$insert_data['O3'] = $data['A'];
			$insert_data['CO2'] = $data['B'];
			$insert_data['SO2'] = $data['C'];
			$insert_data['NO2'] = $data['D'];
			$insert_data['temperatur'] = $data['E'];
			$insert_data['timestamp'] = $data['F'];
			$insert_data['id_sensor'] = $id;
			try
			{
				$this->sensor_data_model->insert($insert_data, FALSE);
			}
			catch(Exception $e) {
				die($e->getMessage());
			}
		}
		$this->session->set_flashdata('message', "sensor berhasil ditambahkan");
		redirect("data_sensor");
	}
	
	function WriteDataSensor($mode,$id)
	{
		 ini_set('memory_limit', '512M');
		$this->load->library('excel');
		$this->load->library('excelwriter');
		$this->load->model("sensor_data_model");
		PHPExcel_Settings::setPdfRenderer(PHPExcel_Settings::PDF_RENDERER_DOMPDF, './application/third_party/pdf');
		$objPHPExcel = new PHPExcel;
		// set default font
		$objPHPExcel->getDefaultStyle()->getFont()->setName('Calibri');
		// set default font size
		$objPHPExcel->getDefaultStyle()->getFont()->setSize(8);
		// create the writer
		if($mode==excel)
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
		else
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "PDF");
		// writer already created the first sheet for us, let's get it
		$objSheet = $objPHPExcel->getActiveSheet();
		// rename the sheet
		$objSheet->setTitle('Sensor');
		// let's bold and size the header font and write the header
		// as you can see, we can specify a range of cells, like here: cells from A1 to A4
		$objSheet->getStyle('A1:G1')->getFont()->setBold(true)->setSize(12);
		// write header
		$objSheet->getCell('A1')->setValue('Nomor Sensor');
		$objSheet->getCell('B1')->setValue('O3');
		$objSheet->getCell('C1')->setValue('CO2');
		$objSheet->getCell('D1')->setValue('SO2');
		$objSheet->getCell('E1')->setValue('NO2');
		$objSheet->getCell('F1')->setValue('Temperatur');
		$objSheet->getCell('G1')->setValue('Timestamp');
		
		$sensor = $this->sensor_data_model->get_many_by('id_sensor',$id);
		for($i=0;$i<count($sensor);$i++)
		{
			$objSheet->getCell('A'.($i+2))->setValue('Sensor'.$sensor[$i]->id_sensor);
			$objSheet->getStyle('A'.($i+2))->getBorders()->getOutline()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
			$objSheet->getCell('B'.($i+2))->setValue($sensor[$i]->O3);
			$objSheet->getStyle('B'.($i+2))->getBorders()->getOutline()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
			$objSheet->getCell('C'.($i+2))->setValue($sensor[$i]->CO2);
			$objSheet->getStyle('C'.($i+2))->getBorders()->getOutline()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
			$objSheet->getCell('D'.($i+2))->setValue($sensor[$i]->SO2);
			$objSheet->getStyle('D'.($i+2))->getBorders()->getOutline()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
			$objSheet->getCell('E'.($i+2))->setValue($sensor[$i]->NO2);
			$objSheet->getStyle('E'.($i+2))->getBorders()->getOutline()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
			$objSheet->getCell('F'.($i+2))->setValue($sensor[$i]->temperatur);
			$objSheet->getStyle('F'.($i+2))->getBorders()->getOutline()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
			$objSheet->getCell('G'.($i+2))->setValue($sensor[$i]->timestamp);
			$objSheet->getStyle('G'.($i+2))->getBorders()->getOutline()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
		}
		// create medium border around the table
		$objSheet->getStyle('A1:G'.(count($sensor)+2))->getBorders()->getOutline()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);
		// autosize the columns
		$objSheet->getColumnDimension('A')->setAutoSize(true);
		$objSheet->getColumnDimension('B')->setAutoSize(true);
		$objSheet->getColumnDimension('C')->setAutoSize(true);
		$objSheet->getColumnDimension('D')->setAutoSize(true);
		$objSheet->getColumnDimension('E')->setAutoSize(true);
		$objSheet->getColumnDimension('F')->setAutoSize(true);
		$objSheet->getColumnDimension('G')->setAutoSize(true);
		// write the file
		if($mode==excel)
		{
			$objWriter->save('./assets/uploads/Data_Sensor.xlsx');
			redirect("./assets/uploads/Data_Sensor.xlsx");
		}
		else
		{
			$objWriter->save('./assets/uploads/Data_Sensor.pdf');
			redirect("./assets/uploads/Data_Sensor.pdf");
		}
	}
	
}
