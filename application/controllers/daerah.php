<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Daerah extends Auth_controller {

    function __construct() {
        parent::__construct();
        $this->load->model("daerah_model");
		$this->load->model("wilayah_model");
    }

    function index() {
        $this->daerah_list();
    }

    function daerah_list() {
        //array of user object
        $data['page_heading'] = 'Daerah management';
        $data['daerah'] = $this->daerah_model->get_all_list();
		//$data['wilayah'] = $this->wilayah_model->get_all();
        $this->template->display('daerah/daerah_list', $data);
    }

    function add() {
        $this->load->library('form_validation');

		//siapin config buat petanya
        //$gmap_conf = $this->config->item('gmap');   //default config buat api key
		
        $data['page_heading'] = 'Daerah management';

        //validate form input
        $this->form_validation->set_rules('id_wilayah', 'id_wilayah', 'required|xss_clean');
		$this->form_validation->set_rules('nama_daerah', 'nama_daerah', 'required|xss_clean');
		$this->form_validation->set_rules('nomor_daerah', 'nomor_daerah', 'required|xss_clean');
		$insert_data = array();
        if ($this->form_validation->run() === TRUE) {
            $insert_data['nama_daerah'] = $this->input->post('nama_daerah');
			$insert_data['id_wilayah'] = $this->input->post('id_wilayah');
			$insert_data['nomor_daerah'] = $this->input->post('nomor_daerah');
			//$insert_data['lat'] = $this->input->post('lat');
			//$insert_data['lang'] = $this->input->post('lang');
			$insert_data['is_hidden'] = $this->input->post('is_hidden')=='default'?1:0;
        }
		
        if ($this->form_validation->run() == true && $this->daerah_model->insert($insert_data, FALSE)) {
            //check to see if we are creating the user
            //redirect them back to the admin page
            $this->session->set_flashdata('message', "daerah berhasil ditambahkan");
            redirect("daerah");
        }

		$data['nama_daerah'] = array(
            'name' => 'nama_daerah',
            'id' => 'nama_daerah',
            'type' => 'text',
            'class' => 'form-control'
        );
		
		$data['nomor_daerah'] = array(
            'name' => 'nomor_daerah',
            'id' => 'nomor_daerah',
            'type' => 'text',
            'class' => 'form-control'
        );
		
		$data['is_hidden'] = array(
            'name' => 'is_hidden',
            'id' => 'is_hidden',
			'value'       => 'default',
			'checked'     => FALSE,
            'class' => 'form-control'
        );
		
		
		$data['id_wilayah'] = $this->wilayah_model->get_dropdown();
		$data['id_wilayah_opt'] = 'class="form-control"';
		
		//buat petanya
        //$this->googlemaps->initialize($gmap_conf);
        //masukin ke variabel buat dikirim ke view
		/*$this->googlemaps->onclick = "	var latitude = event.latLng.lat();
										var longitude = event.latLng.lng();
										$('#lat').val(latitude);
										$('#lang').val(longitude);
										";
        $data['map'] = $this->googlemaps->create_map();
        $data['add_script'] = $this->load->view('daerah/add_script',$data,true);
		*/
        $this->template->display('daerah/add', $data);
    }

    function edit($id) {
        $this->load->library('form_validation');
		
		//siapin config buat petanya
        //$gmap_conf = $this->config->item('gmap');   //default config buat api key
		
        $data['page_heading'] = 'Daerah management';

        $daerah = $this->daerah_model->get($id);

        //validate form input
        $this->form_validation->set_rules('id_wilayah', 'id_wilayah', 'required|xss_clean');
		$this->form_validation->set_rules('nama_daerah', 'nama_daerah', 'required|xss_clean');
		$this->form_validation->set_rules('nomor_daerah', 'nomor_daerah', 'required|xss_clean');

        if (isset($_POST) && !empty($_POST)) {

            $upd_data = array();
			$upd_data['nama_daerah'] = $this->input->post('nama_daerah');
			$upd_data['id_wilayah'] = $this->input->post('id_wilayah');
			$upd_data['nomor_daerah'] = $this->input->post('nomor_daerah');
			$upd_data['is_hidden'] = $this->input->post('is_hidden')=='default'?1:0;
			$upd_data['id'] = $id;
			if($upd_data['is_hidden']==1)$this->daerah_model->update_all(array('is_hidden'=>0));
            if ($this->form_validation->run() === TRUE) {
                $this->daerah_model->update_daerah($upd_data);

                $this->session->set_flashdata('message', "Edit daerah success");

                redirect(base_url() . 'daerah');
            }
        }

		$data['id_wilayah'] = $this->wilayah_model->get_dropdown();
		$data['id_wilayah_opt'] = 'class="form-control"';
		$data['id_wilayah_selected'] = $daerah->id_wilayah;
		
		$data['nama_daerah'] = array(
            'name' => 'nama_daerah',
            'id' => 'nama_daerah',
			'value' => $this->form_validation->set_value('nama_daerah', $daerah->nama_daerah),
            'type' => 'text',
            'class' => 'form-control'
        );
		
		$data['nomor_daerah'] = array(
            'name' => 'nomor_daerah',
            'id' => 'nomor_daerah',
			'value' => $this->form_validation->set_value('nomor_daerah', $daerah->nomor_daerah),
            'type' => 'text',
            'class' => 'form-control'
        );
		
		$data['is_hidden'] = array(
            'name' => 'is_hidden',
            'id' => 'is_hidden',
			'value'       => 'default',
			'checked'     => $daerah->is_hidden==1,
            'class' => 'form-control'
        );
		

        $data['id'] = $id;

		//$gmap_conf['center']=$daerah->lat.', '.$daerah->lang;
		
		//buat petanya
        //$this->googlemaps->initialize($gmap_conf);
        //masukin ke variabel buat dikirim ke view
		/*$this->googlemaps->onclick = "	var latitude = event.latLng.lat();
										var longitude = event.latLng.lng();
										$('#lat').val(latitude);
										$('#lang').val(longitude);
										";
        $data['map'] = $this->googlemaps->create_map();
		*/
        $this->template->display('daerah/edit', $data);
    }

    function view($id) {
        $data['page_heading'] = 'Daerah management';

        $data['daerah'] = $this->daerah_model->get($id);
		$data['wilayah'] = $this->wilayah_model->get($id);

        $this->template->display('daerah/view', $data);
    }

    function delete($id) {
        $this->daerah_model->delete($id);
        $this->session->set_flashdata('message', "daerah sudah dihapus");
        redirect(base_url() . 'daerah');
    }
	
	function do_upload($id)
	{
		$config['upload_path'] = './assets/uploads/';
		$config['allowed_types'] = 'xls|xlsx';
		$config['overwrite'] = true;
		$file = $_FILES['userfile']['name'];
		$ext = substr(strrchr($file, '.'), 1); 
		$config['file_name'] = 'temp.'.$ext;
		$this->load->library('upload', $config);
		
		if ( ! $this->upload->do_upload())
		{
			$this->session->set_flashdata('message', $this->upload->display_errors());
			
			//redirect(base_url() . 'wilayah');
		}
		else
		{
			//$this->session->set_flashdata('message', "masukan excel sensor success");
			$this->excelReadSensor($config['upload_path'].$config['file_name'],$id);
			//redirect(base_url() . 'wilayah');
		}
	}
	
	function excelReadSensor($inputFilePath,$id)
	{
		$this->load->model("sensor_model");
		$this->load->library('excel');

		//$inputFileName = './sampleData/example1.xls';

		//  Read your Excel workbook
		try {
			
			$inputFileType = PHPExcel_IOFactory::identify($inputFilePath);
			$objReader = PHPExcel_IOFactory::createReader($inputFileType);
			$objPHPExcel = $objReader->load($inputFilePath);
		} catch(Exception $e) {
			die('Error loading file "'.pathinfo($inputFilePath,PATHINFO_BASENAME).'": '.$e->getMessage());
		}

		//  Get worksheet dimensions
		//$sheet = $objPHPExcel->getSheet(0); 
		$sheetData = $objPHPExcel->getSheet(0)->toArray(null,true,true,true);
		//$highestRow = $sheet->getHighestRow(); 
		//$highestColumn = $sheet->getHighestColumn();
		foreach ($sheetData as $data) {
			$insert_data['lat'] = $data['A'];
			$insert_data['lang'] = $data['B'];
			$insert_data['id_daerah'] = $id;
			try
			{
				$this->sensor_model->insert($insert_data, FALSE);
			}
			catch(Exception $e) {
				die($e->getMessage());
			}
		}
		$this->session->set_flashdata('message', "daerah berhasil ditambahkan");
		redirect("sensor");
	}
	
	function WriteSensor($mode,$id)
	{
		$this->load->library('excel');
		$this->load->library('excelwriter');
		$this->load->model("sensor_model");
		PHPExcel_Settings::setPdfRenderer(PHPExcel_Settings::PDF_RENDERER_DOMPDF, './application/third_party/pdf');
		$objPHPExcel = new PHPExcel;
		// set default font
		$objPHPExcel->getDefaultStyle()->getFont()->setName('Calibri');
		// set default font size
		$objPHPExcel->getDefaultStyle()->getFont()->setSize(8);
		// create the writer
		if($mode==excel)
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
		else
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "PDF");
		// writer already created the first sheet for us, let's get it
		$objSheet = $objPHPExcel->getActiveSheet();
		// rename the sheet
		$objSheet->setTitle('Sensor');
		// let's bold and size the header font and write the header
		// as you can see, we can specify a range of cells, like here: cells from A1 to A4
		$objSheet->getStyle('A1:C1')->getFont()->setBold(true)->setSize(12);
		// write header
		$objSheet->getCell('A1')->setValue('Nomor Sensor');
		$objSheet->getCell('B1')->setValue('Latitude');
		$objSheet->getCell('C1')->setValue('Longitude');
		
		$sensor = $this->sensor_model->get_many_by('id_daerah',$id);
		for($i=0;$i<count($sensor);$i++)
		{
			$objSheet->getCell('A'.($i+2))->setValue('Sensor'.$sensor[$i]->id);
			$objSheet->getStyle('A'.($i+2))->getBorders()->getOutline()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
			$objSheet->getCell('B'.($i+2))->setValue($sensor[$i]->lat);
			$objSheet->getStyle('B'.($i+2))->getBorders()->getOutline()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
			$objSheet->getCell('C'.($i+2))->setValue($sensor[$i]->lang);
			$objSheet->getStyle('C'.($i+2))->getBorders()->getOutline()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
		}
		// create medium border around the table
		$objSheet->getStyle('A1:C'.(count($sensor)+2))->getBorders()->getOutline()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);
		// autosize the columns
		$objSheet->getColumnDimension('A')->setAutoSize(true);
		$objSheet->getColumnDimension('B')->setAutoSize(true);
		$objSheet->getColumnDimension('C')->setAutoSize(true);
		// write the file
		if($mode==excel)
		{
			$objWriter->save('./assets/uploads/Sensor.xlsx');
			redirect("./assets/uploads/Sensor.xlsx");
		}
		else
		{
			$objWriter->save('./assets/uploads/Sensor.pdf');
			redirect("./assets/uploads/Sensor.pdf");
		}
	}

}
