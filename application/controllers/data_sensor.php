<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Data_sensor extends Auth_controller {

    function __construct() {
        parent::__construct();
        $this->load->model("sensor_data_model");
		$this->load->model("sensor_model");
    }

    function index() {
        $this->data_sensor_list();
    }

    function data_sensor_list() {
        //array of user object
        $data['page_heading'] = 'Data Sensor management';
        $data['data_sensor'] = $this->sensor_data_model->get_all();
		//$data['wilayah'] = $this->wilayah_model->get_all();
        $this->template->display('data_sensor/data_sensor_list', $data);
    }

    function add() {
        $this->load->library('form_validation');

		
        $data['page_heading'] = 'Data Sensor management';

        //validate form input
        $this->form_validation->set_rules('id_sensor', 'id_sensor', 'required|xss_clean');
		$this->form_validation->set_rules('O3', 'O3', 'required|xss_clean');
		$this->form_validation->set_rules('CO2', 'CO2', 'required|xss_clean');
		$this->form_validation->set_rules('SO2', 'SO2', 'required|xss_clean');
		$this->form_validation->set_rules('NO2', 'NO2', 'required|xss_clean');
		$this->form_validation->set_rules('temperatur', 'temperatur', 'required|xss_clean');
		$this->form_validation->set_rules('timestamp', 'timestamp', 'required|xss_clean');
		$insert_data = array();
        if ($this->form_validation->run() === TRUE) {
            $insert_data['id_sensor'] = $this->input->post('id_sensor');
			$insert_data['O3'] = $this->input->post('O3');
			$insert_data['CO2'] = $this->input->post('CO2');
			$insert_data['SO2'] = $this->input->post('SO2');
			$insert_data['NO2'] = $this->input->post('NO2');
			$insert_data['temperatur'] = $this->input->post('temperatur');
			$insert_data['timestamp'] = $this->input->post('timestamp');
        }
		
        if ($this->form_validation->run() == true && $this->sensor_data_model->insert($insert_data, FALSE)) {
            //check to see if we are creating the user
            //redirect them back to the admin page
            $this->session->set_flashdata('message', "sensor berhasil ditambahkan");
            redirect("data_sensor");
        }

		$data['O3'] = array(
            'name' => 'O3',
            'id' => 'O3',
            'type' => 'text',
            'class' => 'form-control'
        );
		
		$data['CO2'] = array(
            'name' => 'CO2',
            'id' => 'CO2',
            'type' => 'text',
            'class' => 'form-control'
        );
		
		$data['SO2'] = array(
            'name' => 'SO2',
            'id' => 'SO2',
            'type' => 'text',
            'class' => 'form-control'
        );
		
		$data['NO2'] = array(
            'name' => 'NO2',
            'id' => 'NO2',
            'type' => 'text',
            'class' => 'form-control'
        );
		
		$data['temperatur'] = array(
            'name' => 'temperatur',
            'id' => 'temperatur',
            'type' => 'text',
            'class' => 'form-control'
        );
		
		$data['timestamp'] = array(
            'name' => 'timestamp',
            'id' => 'timestamp',
            'type' => 'text',
            'class' => 'form-control'
        );
		
		$data['id_sensor'] = $this->sensor_model->get_dropdown();
		$data['id_sensor_opt'] = 'class="form-control"';
		$data['add_script'] = $this->load->view('data_sensor/add_script',$data,true);
        $this->template->display('data_sensor/add', $data);
    }

    function edit($id) {
        $this->load->library('form_validation');
		
        $data['page_heading'] = 'Data Sensor management';

        $data_sensor = $this->sensor_data_model->get($id);

        //validate form input
        $this->form_validation->set_rules('id_sensor', 'id_sensor', 'required|xss_clean');
		$this->form_validation->set_rules('O3', 'O3', 'required|xss_clean');
		$this->form_validation->set_rules('CO2', 'CO2', 'required|xss_clean');
		$this->form_validation->set_rules('SO2', 'SO2', 'required|xss_clean');
		$this->form_validation->set_rules('NO2', 'NO2', 'required|xss_clean');
		$this->form_validation->set_rules('temperatur', 'temperatur', 'required|xss_clean');
		$this->form_validation->set_rules('timestamp', 'timestamp', 'required|xss_clean');

        if (isset($_POST) && !empty($_POST)) {

            $upd_data = array();
			$upd_data['id_sensor'] = $this->input->post('id_sensor');
			$upd_data['O3'] = $this->input->post('O3');
			$upd_data['CO2'] = $this->input->post('CO2');
			$upd_data['SO2'] = $this->input->post('SO2');
			$upd_data['NO2'] = $this->input->post('NO2');
			$upd_data['temperatur'] = $this->input->post('temperatur');
			$upd_data['timestamp'] = $this->input->post('timestamp');
			
			if ($this->form_validation->run() === TRUE) {
                $this->sensor_data_model->update_daerah($upd_data);

                $this->session->set_flashdata('message', "Edit sensor success");

                redirect(base_url() . 'data_sensor');
            }
        }

		$data['id_sensor'] = $this->sensor_model->get_dropdown();
		$data['id_sensor_opt'] = 'class="form-control"';
		$data['id_sensor_selected'] = $data_sensor->id_sensor;
		
		$data['O3'] = array(
            'name' => 'O3',
            'id' => 'O3',
			'value' => $this->form_validation->set_value('O3', $data_sensor->O3),
            'type' => 'text',
            'class' => 'form-control'
        );
		
		$data['CO2'] = array(
            'name' => 'CO2',
            'id' => 'CO2',
			'value' => $this->form_validation->set_value('CO2', $data_sensor->CO2),
            'type' => 'text',
            'class' => 'form-control'
        );
		
		$data['SO2'] = array(
            'name' => 'SO2',
            'id' => 'SO2',
			'value' => $this->form_validation->set_value('SO2', $data_sensor->SO2),
            'type' => 'text',
            'class' => 'form-control'
        );
		
		$data['NO2'] = array(
            'name' => 'NO2',
            'id' => 'NO2',
			'value' => $this->form_validation->set_value('NO2', $data_sensor->NO2),
            'type' => 'text',
            'class' => 'form-control'
        );
		
		$data['temperatur'] = array(
            'name' => 'temperatur',
            'id' => 'temperatur',
			'value' => $this->form_validation->set_value('temperatur', $data_sensor->temperatur),
            'type' => 'text',
            'class' => 'form-control'
        );
		
		$data['timestamp'] = array(
            'name' => 'timestamp',
            'id' => 'timestamp',
			'value' => $this->form_validation->set_value('timestamp', $data_sensor->timestamp),
            'type' => 'text',
            'class' => 'form-control'
        );
		

        $data['id'] = $id;
		$data['add_script'] = $this->load->view('data_sensor/add_script',$data,true);
        $this->template->display('data_sensor/edit', $data);
    }

    function view($id) {
        $data['page_heading'] = 'Daerah management';

        $data['daerah'] = $this->daerah_model->get($id);
		$data['wilayah'] = $this->wilayah_model->get($id);

        $this->template->display('daerah/view', $data);
    }

    function delete($id) {
        $this->daerah_model->delete($id);
        $this->session->set_flashdata('message', "daerah sudah dihapus");
        redirect(base_url() . 'wilayah');
    }

	function WriteDataSensor($mode,$id)
	{
		$this->load->library('excel');
		$this->load->library('excelwriter');
		$this->load->model("sensor_model");
		PHPExcel_Settings::setPdfRenderer(PHPExcel_Settings::PDF_RENDERER_DOMPDF, './application/third_party/pdf');
		$objPHPExcel = new PHPExcel;
		// set default font
		$objPHPExcel->getDefaultStyle()->getFont()->setName('Calibri');
		// set default font size
		$objPHPExcel->getDefaultStyle()->getFont()->setSize(8);
		// create the writer
		if($mode==excel)
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
		else
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "PDF");
		// writer already created the first sheet for us, let's get it
		$objSheet = $objPHPExcel->getActiveSheet();
		// rename the sheet
		$objSheet->setTitle('Sensor');
		// let's bold and size the header font and write the header
		// as you can see, we can specify a range of cells, like here: cells from A1 to A4
		$objSheet->getStyle('A1:C1')->getFont()->setBold(true)->setSize(12);
		// write header
		$objSheet->getCell('A1')->setValue('Nomor Sensor');
		$objSheet->getCell('B1')->setValue('Latitude');
		$objSheet->getCell('C1')->setValue('Longitude');
		
		$sensor = $this->sensor_model->get_many_by('id_daerah',$id);
		for($i=0;$i<=count($sensor);$i++)
		{
			$objSheet->getCell('A'.($i+2))->setValue('Sensor'.$sensor[$i]->id);
			$objSheet->getStyle('A'.($i+1))->getBorders()->getOutline()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
			$objSheet->getCell('B'.($i+2))->setValue($sensor[$i]->lat);
			$objSheet->getStyle('B'.($i+1))->getBorders()->getOutline()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
			$objSheet->getCell('C'.($i+2))->setValue($sensor[$i]->lang);
			$objSheet->getStyle('C'.($i+1))->getBorders()->getOutline()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
		}
		// create medium border around the table
		$objSheet->getStyle('A1:C'.(count($daerah)+1))->getBorders()->getOutline()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);
		// autosize the columns
		$objSheet->getColumnDimension('A')->setAutoSize(true);
		$objSheet->getColumnDimension('B')->setAutoSize(true);
		$objSheet->getColumnDimension('C')->setAutoSize(true);
		// write the file
		if($mode==excel)
		{
			$objWriter->save('./assets/uploads/Sensor.xlsx');
			redirect("./assets/uploads/Sensor.xlsx");
		}
		else
		{
			$objWriter->save('./assets/uploads/Sensor.pdf');
			redirect("./assets/uploads/Sensor.pdf");
		}
	}
	
}
