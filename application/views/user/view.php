<div class="panel panel-default">
    <div class="panel-heading">
        <i class="fa fa-eye"></i> View user
    </div>
    <div class="panel-body">
        <div class="form-group">
            <label>Username</label>
            : <?php echo $user->username?>
        </div>
        <div class="form-group">
            <label>First name</label>
            : <?php echo $user->first_name?>
        </div>
        <div class="form-group">
            <label>Last name</label>
            : <?php echo $user->last_name?>
        </div>
        <div class="form-group">
            <label>Email</label>
            : <?php echo $user->email?>
        </div>
        <div class="form-group">
            <a href="<?php echo base_url()?>user" class="btn btn-warning">Kembali</a>
        </div>
    </div>
</div>