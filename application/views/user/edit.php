<?php if(null!=validation_errors()){?>
<div class="alert alert-info alert-danger">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <?php echo validation_errors(); ?>
</div>
<?php } ?>

<div class="panel panel-default">
    <div class="panel-heading">
        <i class="fa fa-edit"></i> Edit user
    </div>
    <!-- /.panel-heading -->
    <div class="panel-body">

        <?php echo form_open(uri_string()); ?>

        <div class="form-group">
            <label>First name</label>
            <?php echo form_input($first_name); ?>
        </div>

        <div class="form-group">
            <label>Last name</label>
            <?php echo form_input($last_name); ?>
        </div>


        <div class="form-group">
            <label>Password</label>
            <?php echo form_input($password); ?>
        </div>

        <div class="form-group">
            <label>Password confirm</label>
            <?php echo form_input($password_confirm); ?>
        </div>

        <?php echo form_hidden('id', $user->id); ?>

        <div class="form-group">
            <button type="submit" class="btn btn-primary">Submit</button>
            <a href="<?php echo base_url()?>user" class="btn btn-warning">Batal</a>
        </div>

        <?php echo form_close(); ?>

    </div>
    <!-- /.panel-body -->
</div>