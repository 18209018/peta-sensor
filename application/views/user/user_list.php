<div class="panel panel-default">
    <div class="panel-heading">
        <i class="fa fa-users"></i> Daftar user
    </div>
    <!-- /.panel-heading -->
    <div class="panel-body">
        <div class="row">
            <div class="text-center"><a class="btn btn-info" href="<?php echo base_url() ?>user/add"><i class="fa fa-plus"></i>Tambah user</a></div>
        </div>
        <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover" id="dataTables">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Username</th>
                        <th>Email</th>
                        <th>Nama</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $no = 1 ?>
                    <?php foreach ($users as $user) { ?>
                        <tr>
                            <td><?php echo $no++ ?></td>
                            <td><?php echo $user->username ?></td>
                            <td><?php echo $user->email ?></td>
                            <td><?php echo $user->first_name . ' ' . $user->last_name ?></td>
                            <td>
                                <div class="btn-group">
                                    <a href="<?php echo base_url() ?>user/view/<?php echo $user->id ?>" class="btn btn-sm btn-info"><i class="fa fa-eye"></i></a> 
                                    <a href="<?php echo base_url() ?>user/edit/<?php echo $user->id ?>" class="btn btn-sm btn-info"><i class="fa fa-edit"></i></a>
                                    <a href="<?php echo base_url() ?>user/delete/<?php echo $user->id ?>" class="btn_delete btn btn-sm btn-danger"><i class="fa fa-times"></i></a>
                                </div>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
        <div class="row">
            <div class="text-center"><a class="btn btn-info" href="<?php echo base_url() ?>user/add"><i class="fa fa-plus"></i>Tambah user</a></div>
        </div>
    </div>
    <!-- /.panel-body -->
</div>