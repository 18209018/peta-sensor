<script>
    function popup_grafik(id_sensor){
        $('#myModal').load('<?php echo base_url()?>sensor_daerah/grafik_sensor/'+id_sensor);
        $('#myModal').modal({
            keyboard: true,
        });
        $('#myModal').modal('show');
    }
    function popup_tabel(id_sensor){
        $('#myModal').modal({
            keyboard: true,
        });
        $('#myModal').modal('show');
        $('#myModal').load('<?php echo base_url()?>sensor_daerah/tabel_sensor/'+id_sensor);
    }
</script>
<style>
.modal-wide .modal-dialog {
  width: 70%; /* or whatever you wish */
}
</style>

<link rel="stylesheet" href="<?php echo base_url()?>assets/css/plugins/jqplot/jquery.jqplot.min.css"/>
<script src="<?php echo base_url()?>assets/js/plugins/jqplot/jquery.jqplot.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/plugins/jqplot/plugins/jqplot.barRenderer.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/plugins/jqplot/plugins/jqplot.categoryAxisRenderer.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/plugins/jqplot/plugins/jqplot.canvasAxisTickRenderer.min.js"></script>

