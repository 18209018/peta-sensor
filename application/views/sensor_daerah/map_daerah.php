<?php echo $map['js']; ?>
<h1 class="page-header">Data Sensor di daerah <?php echo $daerah['nama_daerah'] ?></h1>
<div class="row">
    <div class="col-lg-5">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-list-alt"></i> Daftar sensor di daerah ini
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="dataTables">
                        <thead>
                            <tr>
                                <th width="10%">No</th>
                                <th>Lat</th>
                                <th>Long</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no = 1 ?>
                            <?php foreach ($sensor as $s) { ?>
                                <tr>
                                    <td><?php echo $no++ ?></td>
                                    <td><?php echo $s['lat']?></td>
                                    <td><?php echo $s['lang'] ?></td>
                                    <td>
                                        <button onclick="popup_grafik(<?php echo $s['id']?>)" class="btn btn-primary btn-sm" data-toggle="tooltip" title="View Chart">
                                            <i class="fa fa-bar-chart-o fa-fw"></i>
                                        </button>
                                        <button onclick="popup_tabel(<?php echo $s['id']?>)" class="btn btn-primary btn-sm" data-toggle="tooltip" title="View Table">
                                            <i class="fa fa-list-alt fa-fw"></i>
                                        </button>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-7">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-map-marker"></i> Marker sensor
            </div>
            <div class="panel-body">
                <?php echo $map['html']; ?>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="text-center"><a class="btn btn-primary" href="<?php echo base_url()?>sensor_wilayah/map_wilayah/<?php echo $daerah['id_wilayah'] ?>"><i class="fa fa-arrow-left"></i> Back</a></div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade modal-wide" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"></div><!-- /.modal -->
