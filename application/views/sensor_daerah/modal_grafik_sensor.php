<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title"><i class="fa fa-bar-chart-o fa-fw"></i> Visualisasi Grafik dari Sensor</h4>
        </div>
        <div class="modal-body">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#o3" data-toggle="tab">O3</a></li>
                <li><a href="#co2" data-toggle="tab">CO2</a></li>
                <li><a href="#so2" data-toggle="tab">SO2</a></li>
                <li><a href="#no2" data-toggle="tab">NO2</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="o3">
                    <div id="graf_o3"></div>
                </div>
                <div class="tab-pane" id="co2">
                    <div id="graf_co2"></div>
                </div>
                <div class="tab-pane" id="so2">
                    <div id="graf_so2"></div>
                </div>
                <div class="tab-pane" id="no2">
                    <div id="graf_no2"></div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
    </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
<script>
    var s1 = <?php echo json_encode($o3['val'],JSON_NUMERIC_CHECK)?>;
    var ticks = <?php echo json_encode($o3['label'],JSON_NUMERIC_CHECK)?>;
    var s2 = <?php echo json_encode($co2['val'],JSON_NUMERIC_CHECK)?>;
    var s3 = <?php echo json_encode($so2['val'],JSON_NUMERIC_CHECK)?>;
    var s4 = <?php echo json_encode($no2['val'],JSON_NUMERIC_CHECK)?>;
    var plot1 = $.jqplot('graf_o3', [s1], {
        width: 900,
        // The "seriesDefaults" option is an options object that will
        // be applied to all series in the chart.
        seriesDefaults:{
            renderer:$.jqplot.LineRenderer,
            rendererOptions: {fillToZero: true}
        },
        // Custom labels for the series are specified with the "label"
        // option on the series option.  Here a series option object
        // is specified for each series.
        series:[
            {label:'O3'}
        ],
        // Show the legend and put it outside the grid, but inside the
        // plot container, shrinking the grid to accomodate the legend.
        // A value of "outside" would not shrink the grid and allow
        // the legend to overflow the container.
        legend: {
            show: true,
            placement: 'outsideGrid'
        },

        axes: {
            // Use a category axis on the x axis and use our custom ticks.
            xaxis: {
                renderer: $.jqplot.CategoryAxisRenderer,
                ticks: ticks
            },
            // Pad the y axis just a little so bars can get close to, but
            // not touch, the grid boundaries.  1.2 is the default padding.
            yaxis: {
                pad: 1.05,
                tickOptions: {formatString: '$%d'}
            }
        }
    });
    
    var plot2 = $.jqplot('graf_co2', [s2], {
        width: 900,
        // The "seriesDefaults" option is an options object that will
        // be applied to all series in the chart.
        seriesDefaults:{
            renderer:$.jqplot.LineRenderer,
            rendererOptions: {fillToZero: true}
        },
        // Custom labels for the series are specified with the "label"
        // option on the series option.  Here a series option object
        // is specified for each series.
        series:[
            {label:'CO2'}
        ],
        seriesColors:['#15134b'],
        // Show the legend and put it outside the grid, but inside the
        // plot container, shrinking the grid to accomodate the legend.
        // A value of "outside" would not shrink the grid and allow
        // the legend to overflow the container.
        legend: {
            show: true,
            placement: 'outsideGrid'
        },
        axes: {
            // Use a category axis on the x axis and use our custom ticks.
            xaxis: {
                renderer: $.jqplot.CategoryAxisRenderer,
                ticks: ticks
            },
            // Pad the y axis just a little so bars can get close to, but
            // not touch, the grid boundaries.  1.2 is the default padding.
            yaxis: {
                pad: 1.05,
                tickOptions: {formatString: '$%d'}
            }
        }
    });
    
    var plot3 = $.jqplot('graf_so2', [s3], {
        width: 900,
        // The "seriesDefaults" option is an options object that will
        // be applied to all series in the chart.
        seriesDefaults:{
            renderer:$.jqplot.LineRenderer,
            rendererOptions: {fillToZero: true}
        },
        // Custom labels for the series are specified with the "label"
        // option on the series option.  Here a series option object
        // is specified for each series.
        series:[
            {label:'SO2'}
        ],
        seriesColors:['#85802b'],
        // Show the legend and put it outside the grid, but inside the
        // plot container, shrinking the grid to accomodate the legend.
        // A value of "outside" would not shrink the grid and allow
        // the legend to overflow the container.
        legend: {
            show: true,
            placement: 'outsideGrid'
        },
        axes: {
            // Use a category axis on the x axis and use our custom ticks.
            xaxis: {
                renderer: $.jqplot.CategoryAxisRenderer,
                ticks: ticks
            },
            // Pad the y axis just a little so bars can get close to, but
            // not touch, the grid boundaries.  1.2 is the default padding.
            yaxis: {
                pad: 1.05,
                tickOptions: {formatString: '$%d'}
            }
        }
    });
    
    var plot4 = $.jqplot('graf_no2', [s4], {
        width: 900,
        // The "seriesDefaults" option is an options object that will
        // be applied to all series in the chart.
        seriesDefaults:{
            renderer:$.jqplot.LineRenderer,
            rendererOptions: {fillToZero: true}
        },
        // Custom labels for the series are specified with the "label"
        // option on the series option.  Here a series option object
        // is specified for each series.
        series:[
            {label:'NO2'}
        ],
        seriesColors:['#65834b'],
        // Show the legend and put it outside the grid, but inside the
        // plot container, shrinking the grid to accomodate the legend.
        // A value of "outside" would not shrink the grid and allow
        // the legend to overflow the container.
        legend: {
            show: true,
            placement: 'outsideGrid'
        },
        axes: {
            // Use a category axis on the x axis and use our custom ticks.
            xaxis: {
                renderer: $.jqplot.CategoryAxisRenderer,
                ticks: ticks
            },
            // Pad the y axis just a little so bars can get close to, but
            // not touch, the grid boundaries.  1.2 is the default padding.
            yaxis: {
                pad: 1.05,
                tickOptions: {formatString: '$%d'}
            }
        }
    });
    $('#myModal').on('shown.bs.modal', function () {
        if (plot1._drawCount === 0) {
                plot1.replot();
            }
    })
    
    $('a[href="#o3"]').on('shown.bs.tab', function(e) {
            if (plot1._drawCount === 0) {
                plot1.replot();
            }
    });
    $('a[href="#co2"]').on('shown.bs.tab', function(e) {
            if (plot2._drawCount === 0) {
                plot2.replot();
            }
    });
    $('a[href="#so2"]').on('shown.bs.tab', function(e) {
            if (plot3._drawCount === 0) {
                plot3.replot();
            }
    });
    $('a[href="#no2"]').on('shown.bs.tab', function(e) {
            if (plot4._drawCount === 0) {
                plot4.replot();
            }
    });
    
</script>
