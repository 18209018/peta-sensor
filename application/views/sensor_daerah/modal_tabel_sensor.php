<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title"><i class="fa fa-bar-chart-o fa-fw"></i> Tabel Data dari Sensor</h4>
        </div>
        <div class="modal-body">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover" id="dataTables">
                    <thead>
                        <tr>
                            <th width="10%">No</th>
                            <th>Waktu</th>
                            <th>O3</th>
                            <th>CO2</th>
                            <th>SO2</th>
                            <th>NO2</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $no=1?>
                        <?php foreach($data_sensor as $d){?>
                        <tr>
                            <td><?php echo $no++?></td>
                            <td><?php echo $d['time']?></td>
                            <td><?php echo $d['O3']?></td>
                            <td><?php echo $d['CO2']?></td>
                            <td><?php echo $d['SO2']?></td>
                            <td><?php echo $d['NO2']?></td>
                        </tr>
                        <?php if($no>24){break;}?>
                        <?php }?>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
    </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
