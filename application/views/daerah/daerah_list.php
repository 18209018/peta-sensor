<div class="panel panel-default">
    <div class="panel-heading">
        <i class="fa fa-users"></i> Daftar daerah
    </div>
    <!-- /.panel-heading -->
    <div class="panel-body">
        <div class="row">
            <div class="text-center"><a class="btn btn-info" href="<?php echo base_url() ?>daerah/add"><i class="fa fa-plus"></i>Tambah daerah</a></div>
        </div>
        <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover" id="dataTables">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Wilayah</th>
						<th>Daerah</th>
						<th>Nomor</th>
						<th>Ditampilkan</th>
						<th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $no = 0 ?>
                    <?php 
					if($daerah)
					{
						foreach ($daerah as $daerah1) { ?>
							<tr>
								<td><?php echo $no++ ?></td>
								<td><?php echo $daerah1->nama_wilayah ?></td>
								<td><?php echo $daerah1->nama_daerah ?></td>
								<td><?php echo $daerah1->nomor_daerah ?></td>
								<td><?php echo $daerah1->is_hidden==1?"ditampilkan":"tidak ditampilkan" ?></td>
								<td>
									<div class="btn-group">
										<a href="<?php echo base_url() ?>daerah/view/<?php echo $daerah1->id_daerah ?>" class="btn btn-sm btn-info"><i class="fa fa-eye"></i></a> 
										<a href="<?php echo base_url() ?>daerah/edit/<?php echo $daerah1->id_daerah ?>" class="btn btn-sm btn-info"><i class="fa fa-edit"></i></a>
										<a href="<?php echo base_url() ?>daerah/delete/<?php echo $daerah1->id_daerah ?>" class="btn_delete btn btn-sm btn-danger"><i class="fa fa-times"></i></a>
										<a href="<?php echo base_url() ?>daerah/WriteSensor/excel/<?php echo $daerah1->id ?>" ><i class="fa fa-times">excel</i></a>
										<a href="<?php echo base_url() ?>daerah/WriteSensor/pdf/<?php echo $daerah1->id ?>" ><i class="fa fa-times">pdf</i></a>
									</div>
									<?php echo form_open_multipart('daerah/do_upload/'.$daerah1->id);?>
									<input type="file" name="userfile" size="20" />
									<input type="submit" value="upload" />
									</form>
								</td>
							</tr>
						<?php }
					}?>
                </tbody>
            </table>
        </div>
        <div class="row">
            <div class="text-center"><a class="btn btn-info" href="<?php echo base_url() ?>daerah/add"><i class="fa fa-plus"></i>Tambah daerah</a></div>
        </div>
    </div>
    <!-- /.panel-body -->
</div>