<?php //echo $map['js']; ?>
<?php if(null!=validation_errors()){?>
<div class="alert alert-info alert-danger">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <?php echo validation_errors(); ?>
</div>
<?php } ?>

<div class="panel panel-default">
    <div class="panel-heading">
        <i class="fa fa-plus-circle"></i> Tambah daerah
    </div>
	<div class="panel panel-default">
		<?php //echo $map['html']; ?>
	</div>
    <!-- /.panel-heading -->
    <div class="panel-body">

        <?php echo form_open(uri_string()); ?>

		<div class="form-group">
            <label>Nama Wilayah</label>
            <?php echo form_dropdown('id_wilayah',$id_wilayah,"",$id_wilayah_opt); ?>
        </div>
		
        <div class="form-group">
            <label>Nama Daerah</label>
            <?php echo form_input($nama_daerah); ?>
        </div>
		
		<div class="form-group">
            <label>Nomor Daerah</label>
            <?php echo form_input($nomor_daerah); ?>
        </div>
		
		<div class="form-group">
            <label>Ditampilkan</label>
            <?php echo form_checkbox($is_hidden); ?>
        </div>
        
        <div class="form-group">
            <button type="submit" class="btn btn-primary">Submit</button>
            <a href="<?php echo base_url()?>user" class="btn btn-warning">Batal</a>
        </div>

        <?php echo form_close(); ?>

    </div>
    <!-- /.panel-body -->
</div>
