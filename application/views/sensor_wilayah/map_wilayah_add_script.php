<script src="<?php echo base_url() ?>assets/js/gdropdown.js"></script>
<link href="<?php echo base_url() ?>assets/css/gdropdown.css" rel="stylesheet"/>
<script>
    var heatmap;
    sensor = <?php echo $avg ?>;
    var heatmapDataO3 = [];
    var heatmapDataCO2 = [];
    var heatmapDataSO2 = [];
    var heatmapDataNO2 = [];

    $(sensor).each(function(e, v) {
        var latLng = new google.maps.LatLng(v.lat, v.lang);

        var weightedLoc = {
            location: latLng,
            weight: 0.01 * v.O3
        };

        heatmapDataO3.push(weightedLoc);

        var weightedLoc = {
            location: latLng,
            weight: 1 * v.CO2
        };

        heatmapDataCO2.push(weightedLoc);

        var weightedLoc = {
            location: latLng,
            weight: 0.001 * v.SO2
        };

        heatmapDataSO2.push(weightedLoc);

        var weightedLoc = {
            location: latLng,
            weight: 2 * v.NO2
        };

        heatmapDataNO2.push(weightedLoc);

    });

    $(window).load(function() {
        $('#map_canvas').append('<div class="container">' + $('#dropdown').html() + '</div>');
        $('#dropdown').remove();
        
        heatmap = new google.maps.visualization.HeatmapLayer({
            dissipating: false,
            map: map
        });
        //heatmap.setData(heatmapDataO3);
        $('#opto3').trigger('click');
        $('#mddControl').trigger('click');
    });
    
    function setData(obj,data) {
        $('.dropDownItemDiv').find('i').removeClass('fa-check-square-o');
        $('.dropDownItemDiv').find('i').addClass('fa-square-o');
        $(obj).find('i').removeClass('fa-square-o');
        $(obj).find('i').addClass('fa-check-square-o');
        heatmap.setData(data);
        $('#mddControl').trigger("click");
    }
</script>