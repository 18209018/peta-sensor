<?php echo $map['js']; ?>
<h1 class="page-header">Data Sensor</h1>

<div class="row">
    <div class="col-lg-5">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-list-alt"></i> Daftar daerah di wilayah ini
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="dataTables">
                        <thead>
                            <tr>
                                <th width="10%">No</th>
                                <th>Daerah</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no = 1 ?>
                            <?php foreach ($daerah as $d) { ?>
                                <tr>
                                    <td><?php echo $no++ ?></td>
                                    <td><?php echo $d['nama_daerah'] ?></td>
                                    <td><a href="<?php echo base_url() . 'sensor_daerah/map_daerah/' . $d['id'] ?>" class="btn btn-primary btn-sm"><i class="fa fa-search"></i> View</a></td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-7">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-map-marker"></i> Peta Visualisasi Data Sensor
            </div>
            <div class="panel-body">
                <div id="dropdown" class="container">
                    <div class="dropDownControl" id="mddControl" title="Pilih jenis sensor untuk ditampilkan" onclick="(document.getElementById('mddOptsDiv').style.display == 'block') ? document.getElementById('mddOptsDiv').style.display = 'none' : document.getElementById('mddOptsDiv').style.display = 'block';"">
                        Sensor&nbsp;&nbsp;&nbsp;<i class="fa fa-caret-down"></i>
                    </div>
                    <div class = "dropDownOptionsDiv" id="mddOptsDiv">
                        <div class = "dropDownItemDiv" id="opto3"  title="This acts like a button or click event" onClick="setData(this,heatmapDataO3);">
                            <i class="fa fa-square-o"></i> O3
                        </div>
                        <div class = "dropDownItemDiv" id="optco2" title="This acts like a button or click event" onClick="setData(this,heatmapDataCO2);">
                            <i class="fa fa-square-o"></i> CO2
                        </div>
                        <div class = "dropDownItemDiv" id="optso2" title="This acts like a button or click event" onClick="setData(this,heatmapDataSO2);">
                            <i class="fa fa-square-o"></i> SO2
                        </div>
                        <div class = "dropDownItemDiv" id="optno2" title="This acts like a button or click event" onClick="setData(this,heatmapDataNO2);">
                            <i class="fa fa-square-o"></i> NO2
                        </div>
                        <div style="margin-top: 1px; margin-right: 0px; margin-bottom: 1px; margin-left: 0px; border-top-width: 1px; border-top-style: solid; border-top-color: rgb(235, 235, 235); display: none; "></div>
                    </div>

                </div>
                <?php echo $map['html']; ?>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="text-center"><a class="btn btn-primary" href="<?php echo base_url()?>"><i class="fa fa-arrow-left"></i> Back</a></div>
    </div>
</div>