<!DOCTYPE html>
<html>

    <head>
        <link rel="icon" href="<?php echo base_url()?>assets/images/icon.ico">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title><?php echo (isset($title) ? $title : 'Peta Sensor') ?></title>

    </head>

    <body>