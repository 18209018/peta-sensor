<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav" id="side-menu">
            <li>
                <a href="<?php echo base_url()?>user"><i class="fa fa-users fa-fw"></i> User management</a>
            </li>
			<li>
                <a href="<?php echo base_url()?>wilayah"><i class="fa fa-users fa-fw"></i> Wilayah management</a>
            </li>
			<li>
                <a href="<?php echo base_url()?>daerah"><i class="fa fa-users fa-fw"></i> Daerah management</a>
            </li>
			<li>
                <a href="<?php echo base_url()?>sensor"><i class="fa fa-users fa-fw"></i> Sensor management</a>
            </li>
			<li>
                <a href="<?php echo base_url()?>data_sensor"><i class="fa fa-users fa-fw"></i> Sensor Data management</a>
            </li>
        </ul>
        <!-- /#side-menu -->
    </div>
    <!-- /.sidebar-collapse -->
</nav>
<!-- /.navbar-static-side -->