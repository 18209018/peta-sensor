<?php echo $header ?>
<div id="wrapper">
    <?php echo $navbar ?>
    <?php echo $sidebar ?>

    <div id="page-wrapper">
        <?php if (null != ($this->session->flashdata('message')) && $this->session->flashdata('message') != '') { ?>
            <div class="alert alert-info alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('message'); ?>
            </div>
        <?php } ?>
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header"><?php echo $page_heading ?></h1>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <?php echo $content ?>
            </div>
        </div>
    </div>
</div>
<?php echo $script ?>
<?php echo $footer ?>
