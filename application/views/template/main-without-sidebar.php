<?php echo $header ?>
<div id="wrapper">
    <?php echo $navbar ?>
    <div id="page-wrapper-sidebarless">
        <?php if (null != ($this->session->flashdata('message')) && $this->session->flashdata('message') != '') { ?>
            <div class="alert alert-info alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('message'); ?>
            </div>
        <?php } ?>
        <div class="row">
            <div class="col-lg-12">
                <?php echo $content ?>
            </div>
        </div>
    </div>
</div>
<?php echo $script ?>
<?php echo $footer ?>