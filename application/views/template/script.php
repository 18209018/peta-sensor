<link href="<?php echo base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url() ?>assets/font-awesome/css/font-awesome.css" rel="stylesheet">
<link href="<?php echo base_url() ?>assets/css/sb-admin.css" rel="stylesheet">
<link href="<?php echo base_url() ?>assets/css/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet">
<link href="<?php echo base_url() ?>assets/css/datetimepicker/jquery.datetimepicker.css" rel="stylesheet">
<script src="<?php echo base_url() ?>assets/js/jquery.js"></script>
<script src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>

<script src="<?php echo base_url() ?>assets/js/sb-admin.js"></script>

<script src="<?php echo base_url() ?>assets/js/plugins/dataTables/jquery.dataTables.js"></script>
<script src="<?php echo base_url() ?>assets/js/plugins/dataTables/dataTables.bootstrap.js"></script>
<script src="<?php echo base_url() ?>assets/js/datetimepicker/jquery.datetimepicker.js"></script>

<script>
    $(document).ready(function() {
        $('[data-toggle="tooltip"]').tooltip({'placement': 'bottom'});
        var oTable = $('#dataTables').dataTable();
        $('.btn_delete').on('click',function(){
            return confirm('Anda yakin?');
        });
    });
</script>
<?php 
if(isset($add_script)){
    if(is_array($add_script)){
        foreach($add_script as $script){
            echo $script;
        }
    }else{
        echo $add_script;
    }
}
?>