<div class="panel panel-default">
    <div class="panel-heading">
        <i class="fa fa-users"></i> Daftar sensor
    </div>
    <!-- /.panel-heading -->
    <div class="panel-body">
        <div class="row">
            <div class="text-center"><a class="btn btn-info" href="<?php echo base_url() ?>sensor/add"><i class="fa fa-plus"></i>Tambah sensor</a></div>
			<?php echo form_open_multipart('sensor/do_upload');?>
			<input type="file" name="userfile" size="20" />
			<input type="submit" value="upload" />
        </div>
        <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover" id="dataTables">
                <thead>
                    <tr>
                        <th>No</th>
						<th>Nama Sensor</th>
                        <th>Wilayah</th>
						<th>Daerah</th>
						<th>Nomor Daerah</th>
						<th>Latitude</th>
						<th>Longitude</th>
						<th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $no = 0 ?>
                    <?php 
					if($sensor)
					{
						foreach ($sensor as $sensor1) { ?>
							<tr>
								<td><?php echo $no++ ?></td>
								<td>sensor<?php echo $sensor1->id_sensor_asli ?></td>
								<td><?php echo $sensor1->nama_wilayah ?></td>
								<td><?php echo $sensor1->nama_daerah ?></td>
								<td><?php echo $sensor1->nomor_daerah ?></td>
								<td><?php echo $sensor1->lat ?></td>
								<td><?php echo $sensor1->lang ?></td>
								<td>
									<div class="btn-group">
										<a href="<?php echo base_url() ?>sensor/view/<?php echo $sensor1->id_sensor_asli ?>" class="btn btn-sm btn-info"><i class="fa fa-eye"></i></a> 
										<a href="<?php echo base_url() ?>sensor/edit/<?php echo $sensor1->id_sensor_asli ?>" class="btn btn-sm btn-info"><i class="fa fa-edit"></i></a>
										<a href="<?php echo base_url() ?>sensor/delete/<?php echo $sensor1->id_sensor_asli ?>" class="btn_delete btn btn-sm btn-danger"><i class="fa fa-times"></i></a>
										<a href="<?php echo base_url() ?>sensor/WriteDataSensor/excel/<?php echo $sensor1->id_sensor_asli ?>" ><i class="fa fa-times">excel</i></a>
										<a href="<?php echo base_url() ?>sensor/WriteDataSensor/pdf/<?php echo $sensor1->id_sensor_asli ?>" ><i class="fa fa-times">pdf</i></a>
										
									</div>
									<?php echo form_open_multipart('sensor/do_upload/'.$sensor1->id_sensor_asli);?>
										<input type="file" name="userfile" size="20" />
										<input type="submit" value="upload" />
										</form>
								</td>
							</tr>
						<?php }
					}?>
                </tbody>
            </table>
        </div>
        <div class="row">
            <div class="text-center"><a class="btn btn-info" href="<?php echo base_url() ?>sensor/add"><i class="fa fa-plus"></i>Tambah sensor</a></div>
        </div>
    </div>
    <!-- /.panel-body -->
</div>