<?php 
//echo $add_script;
	echo $map['js'];

?>
<?php if(null!=validation_errors()){?>
<div class="alert alert-info alert-danger">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <?php echo validation_errors(); ?>
</div>
<?php } ?>

<div class="panel panel-default">
    <div class="panel-heading">
        <i class="fa fa-edit"></i> Edit daerah
    </div>
	<div class="panel panel-default">
		<?php echo $map['html']; ?>
	</div>
    <!-- /.panel-heading -->
    <div class="panel-body">

        <?php echo form_open(uri_string()); ?>
		
		<div class="form-group">
            <label>Nama Wilayah</label>
            <?php echo form_dropdown('id_wilayah',$id_wilayah,$id_wilayah_selected,$id_wilayah_opt); ?>
        </div>
		
        <div class="form-group">
            <label>Nama Daerah</label>
            <?php echo form_dropdown('id_daerah',$id_daerah,$id_daerah_selected,$id_daerah_opt); ?>
        </div>
		
		<div class="form-group">
            <label>Latitude</label>
            <?php echo form_input($lat); ?>
        </div>
		
		<div class="form-group">
            <label>Longitude</label>
            <?php echo form_input($lang); ?>
        </div>

        <?php echo form_hidden('id', $id); ?>

        <div class="form-group">
            <button type="submit" class="btn btn-primary">Submit</button>
            <a href="<?php echo base_url()?>daerah" class="btn btn-warning">Batal</a>
        </div>

        <?php echo form_close(); ?>

    </div>
    <!-- /.panel-body -->
</div>