<?php echo $map['js']; ?>
<h1 class="page-header">Select Wilayah</h1>
<div class="form-group">
    <?php echo form_dropdown('select_wilayah', $wilayah, $selected_wilayah,'id="select_wilayah" class="form-control"')?>
</div>
<div class="panel panel-default">
    <div class="panel-body">
        <?php echo $map['html']; ?>
    </div>
</div>
<div class="form-group text-center">
    <button type="button" class="btn btn-primary btn-lg" id="enter">
        <i class="fa fa-sign-in"></i> Enter <i class="fa fa-sign-in fa-rotate-180"></i>
    </button>
</div>