<div class="panel panel-default">
    <div class="panel-heading">
        <i class="fa fa-users"></i> Daftar wilayah
    </div>
    <!-- /.panel-heading -->
    <div class="panel-body">
        <div class="row">
            <div class="text-center"><a class="btn btn-info" href="<?php echo base_url() ?>wilayah/add"><i class="fa fa-plus"></i>Tambah wilayah</a></div>
			<div class="text-center"><a class="btn btn-info" href="<?php echo base_url() ?>wilayah/WriteWilayah/excel"><i class="fa fa-plus"></i>Export excel wilayah</a></div>
			<div class="text-center"><a class="btn btn-info" href="<?php echo base_url() ?>wilayah/WriteWilayah/pdf"><i class="fa fa-plus"></i>Export pdf wilayah</a></div>
			<?php  echo form_open_multipart('wilayah/do_upload');?>
			<input type="file" name="userfile" size="20" />
			<input type="submit" value="upload" />
			</form>
		</div>
        <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover" id="dataTables">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Wilayah</th>
						<th>Default Wilayah</th>
						<th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $no = 0 ?>
                    <?php 
					if($wilayah)
					{
						foreach ($wilayah as $wilayah1) { ?>
							<tr>
								<td><?php echo $no++ ?></td>
								<td><?php echo $wilayah1->nama_wilayah ?></td>
								<td><?php echo $wilayah1->is_default==1?"default":"" ?></td>
								<td>
									<div class="btn-group">
										<a href="<?php echo base_url() ?>wilayah/view/<?php echo $wilayah1->id ?>" class="btn btn-sm btn-info"><i class="fa fa-eye"></i></a> 
										<a href="<?php echo base_url() ?>wilayah/edit/<?php echo $wilayah1->id ?>" class="btn btn-sm btn-info"><i class="fa fa-edit"></i></a>
										<a href="<?php echo base_url() ?>wilayah/delete/<?php echo $wilayah1->id ?>" class="btn_delete btn btn-sm btn-danger"><i class="fa fa-times"></i></a>
										<a href="<?php echo base_url() ?>wilayah/WriteDaerah/excel/<?php echo $wilayah1->id ?>" ><i class="fa fa-times">excel</i></a>
										<a href="<?php echo base_url() ?>wilayah/WriteDaerah/pdf/<?php echo $wilayah1->id ?>" ><i class="fa fa-times">pdf</i></a>
									</div>
									<br />
									<?php echo form_open_multipart('wilayah/do_upload/'.$wilayah1->id);?>
									<input type="file" name="userfile" size="20" />
									<input type="submit" value="upload" />
									</form>
								</td>
							</tr>
						<?php }
					}?>
                </tbody>
            </table>
        </div>
        <div class="row">
            <div class="text-center"><a class="btn btn-info" href="<?php echo base_url() ?>wilayah/add"><i class="fa fa-plus"></i>Tambah wilayah</a></div>
        </div>
    </div>
    <!-- /.panel-body -->
</div>