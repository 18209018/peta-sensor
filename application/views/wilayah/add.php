<?php if(null!=validation_errors()){?>
<div class="alert alert-info alert-danger">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <?php echo validation_errors(); ?>
</div>
<?php } ?>

<div class="panel panel-default">
    <div class="panel-heading">
        <i class="fa fa-plus-circle"></i> Tambah wilayah
    </div>
    <!-- /.panel-heading -->
    <div class="panel-body">

        <?php echo form_open(uri_string()); ?>

        <div class="form-group">
            <label>Wilayah</label>
            <?php echo form_input($wilayah); ?>
        </div>
		
		<div class="form-group">
            <label>Default Wilayah</label>
            <?php echo form_checkbox($is_default); ?>
        </div>
        
        <div class="form-group">
            <button type="submit" class="btn btn-primary">Submit</button>
            <a href="<?php echo base_url()?>user" class="btn btn-warning">Batal</a>
        </div>

        <?php echo form_close(); ?>

    </div>
    <!-- /.panel-body -->
</div>
