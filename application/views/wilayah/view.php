<div class="panel panel-default">
    <div class="panel-heading">
        <i class="fa fa-eye"></i> View wilayah
    </div>
    <div class="panel-body">
        <div class="form-group">
            <label>Nama Wilayah</label>
            : <?php echo $wilayah->nama_wilayah?>
        </div>
        <div class="form-group">
            <a href="<?php echo base_url()?>wilayah" class="btn btn-warning">Kembali</a>
        </div>
    </div>
</div>