<?php //echo $map['js']; ?>
<?php if(null!=validation_errors()){?>
<div class="alert alert-info alert-danger">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <?php echo validation_errors(); ?>
</div>
<?php } ?>

<div class="panel panel-default">
    <div class="panel-heading">
        <i class="fa fa-plus-circle"></i> Tambah Data Sensor
    </div>
	<div class="panel panel-default">
		<?php //echo $map['html']; ?>
	</div>
    <!-- /.panel-heading -->
    <div class="panel-body">

        <?php echo form_open(uri_string()); ?>

		<div class="form-group">
            <label>Nomor Sensor</label>
            <?php echo form_dropdown('id_sensor',$id_sensor,"",$id_sensor_opt); ?>
        </div>
		
        <div class="form-group">
            <label>Nilai O3</label>
            <?php echo form_input($O3); ?>
        </div>
		
		<div class="form-group">
            <label>Nilai CO2</label>
            <?php echo form_input($CO2); ?>
        </div>
		
		<div class="form-group">
            <label>Nilai SO2</label>
            <?php echo form_checkbox($SO2); ?>
        </div>
        
		<div class="form-group">
            <label>Nilai NO2</label>
            <?php echo form_checkbox($NO2); ?>
        </div>
		
		<div class="form-group">
            <label>Nilai temperatur</label>
            <?php echo form_checkbox($temperatur); ?>
        </div>
		
		<div class="form-group">
            <label>timestamp</label>
            <?php echo form_checkbox($timestamp); ?>
        </div>
		
        <div class="form-group">
            <button type="submit" class="btn btn-primary">Submit</button>
            <a href="<?php echo base_url()?>data_sensor" class="btn btn-warning">Batal</a>
        </div>

        <?php echo form_close(); ?>

    </div>
    <!-- /.panel-body -->
</div>
