<div class="panel panel-default">
    <div class="panel-heading">
        <i class="fa fa-eye"></i> View daerah
    </div>
    <div class="panel-body">
        <div class="form-group">
            <label>Nama Wilayah</label>
            : <?php echo $wilayah->nama_wilayah?>
        </div>
		<div class="form-group">
            <label>Nama Daerah</label>
            : <?php echo $daerah->nama_daerah?>
        </div>
		<div class="form-group">
            <label>Nomor Daerah</label>
            : <?php echo $daerah->nomor_daerah?>
        </div>
        <div class="form-group">
            <a href="<?php echo base_url()?>daerah" class="btn btn-warning">Kembali</a>
        </div>
    </div>
</div>