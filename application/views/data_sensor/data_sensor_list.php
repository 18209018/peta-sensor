<div class="panel panel-default">
    <div class="panel-heading">
        <i class="fa fa-users"></i> Daftar Data Sensor
    </div>
    <!-- /.panel-heading -->
    <div class="panel-body">
        <div class="row">
            <div class="text-center"><a class="btn btn-info" href="<?php echo base_url() ?>data_sensor/add"><i class="fa fa-plus"></i>Tambah Data Sensor</a></div>
        </div>
        <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover" id="dataTables">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nomor Sensor</th>
						<th>O3</th>
						<th>CO2</th>
						<th>SO2</th>
						<th>NO2</th>
						<th>Temperatur</th>
						<th>Timestamp</th>
						<th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $no = 0 ?>
                    <?php 
					if($data_sensor)
					{
						foreach ($data_sensor as $data_sensor1) { ?>
							<tr>
								<td><?php echo $no++ ?></td>
								<td>sensor<?php echo $data_sensor1->id_sensor ?></td>
								<td><?php echo $data_sensor1->O3 ?></td>
								<td><?php echo $data_sensor1->CO2 ?></td>
								<td><?php echo $data_sensor1->SO2 ?></td>
								<td><?php echo $data_sensor1->NO2 ?></td>
								<td><?php echo $data_sensor1->temperatur ?></td>
								<td><?php echo $data_sensor1->timestamp ?></td>
								<td>
									<div class="btn-group">
										<a href="<?php echo base_url() ?>data_sensor/view/<?php echo $data_sensor1->id ?>" class="btn btn-sm btn-info"><i class="fa fa-eye"></i></a> 
										<a href="<?php echo base_url() ?>data_sensor/edit/<?php echo $data_sensor1->id ?>" class="btn btn-sm btn-info"><i class="fa fa-edit"></i></a>
										<a href="<?php echo base_url() ?>data_sensor/delete/<?php echo $data_sensor1->id ?>" class="btn_delete btn btn-sm btn-danger"><i class="fa fa-times"></i></a>
									</div>
								</td>
							</tr>
						<?php }
					}?>
                </tbody>
            </table>
        </div>
        <div class="row">
            <div class="text-center"><a class="btn btn-info" href="<?php echo base_url() ?>data_sensor/add"><i class="fa fa-plus"></i>Tambah Data Sensor</a></div>
        </div>
    </div>
    <!-- /.panel-body -->
</div>