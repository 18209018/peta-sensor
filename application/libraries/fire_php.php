<?php

require_once APPPATH.'third_party/FirePHP/fb.php';
require_once APPPATH.'third_party/FirePHP/FirePHP.class.php';

class Fire_php{
    var $obj;
    
    function __construct() {
        $this->obj = FirePHP::init();
    }
    
    function log($data,$msg=''){
        $this->obj->log($data, $msg==''?'Debug : ':$msg);
    }
}
