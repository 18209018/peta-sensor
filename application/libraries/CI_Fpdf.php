<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once(APPPATH . "/third_party/fpdf/fpdf.php");

class CI_Fpdf extends Fpdf {

    public $title;

    function __construct($params) {
        parent::__construct($params['orientation']);
    }

    function daily_pdf($title, $data) {
        $this->title = $title;
        $this->SetMargins('10', '10');
        $this->AliasNbPages();
        $this->AddPage();
        //Document title
        $this->SetFont('Times', 'B', 18);
        $this->Cell(0, 0, $this->title, 0, 0, 'C');
        $this->Ln(7);
        //Well name
        $this->SetFont('Times', 'B', 12);
        $this->Cell(30, 0, 'Well name', 0, 0, 'L');
        $this->SetFont('Times', '', 12);
        $this->Cell(30, 0, ': ' . $data['well']['well_name'], 0, 0, 'L');
        $this->Ln(5);
        //Structure
        $this->SetFont('Times', 'B', 12);
        $this->Cell(30, 0, 'Structure name', 0, 0, 'L');
        $this->SetFont('Times', '', 12);
        $this->Cell(30, 0, ': ' . $data['strc']['structure_name'], 0, 0, 'L');
        $this->Ln(5);
        //Area
        $this->SetFont('Times', 'B', 12);
        $this->Cell(30, 0, 'Area name', 0, 0, 'L');
        $this->SetFont('Times', '', 12);
        $this->Cell(30, 0, ': ' . $data['area']['area_name'], 0, 0, 'L');
        $this->Ln(5);
        //Layer
        $this->SetFont('Times', 'B', 12);
        $this->Cell(30, 0, 'Layer name', 0, 0, 'L');
        $this->SetFont('Times', '', 12);
        $this->Cell(30, 0, ': ' . $data['layer']['layer_name'], 0, 0, 'L');
        $this->Ln(10);
        
        //Tabel data       
        // Header
        $this->SetFillColor(0, 100, 255);
        $this->SetTextColor(255);
        $this->SetDrawColor(128, 0, 0);
        $this->SetLineWidth(.3);
        $this->SetFont('Times','B',9);
        $header = array(
            'No','Date','Gross (barrel)',
            'Water cut (%)','Nett (bopd)','Gas out (Mmscf)',
            'Prod. Status','Choke (mm)','PC',
            'PWH','PFL','PSep','Temp. (F)','Ket'
            );
        $w = array(8,20,25,25,20,27,20,20,16,16,16,16,16,35);
        for ($i = 0; $i < count($header); $i++){
            $this->Cell($w[$i], 7, $header[$i], 1, 0, 'C', true);
        }
        $this->Ln();
        // Data
        $this->SetFillColor(255, 255, 255);
        $this->SetTextColor(0);
        $this->SetFont('Times','',9);
        // Data
        $unit = array(1=>'psi',2=>'ksc');
        $arr_prod = array(1=>'Flowing',2=>'Gas Lift',3=>'ESP');
        $i=1;
        foreach($data['daily_all'] as $row){
            $this->Cell($w[0], 7, $i++, 1, 0, 'L');
            $this->Cell($w[1], 7, reformat_date($row['date_created'], 'Y-m-d', 'd/m/Y'), 1, 0, 'L');
            $this->Cell($w[2], 7, $row['fluid_daily'], 1, 0, 'R');
            $this->Cell($w[3], 7, $row['water_cut_daily'], 1, 0, 'R');
            $this->Cell($w[4], 7, $row['oil_prod'], 1, 0, 'R');
            $this->Cell($w[5], 7, $row['gas_retrieve']+$row['gas_inject'], 1, 0, 'R');
            $this->Cell($w[6], 7, $arr_prod[$row['prod_status']], 1, 0, 'R');
            $this->Cell($w[7], 7, ($row['choke']!='')?$row['choke']:'', 1, 0, 'R');
            $this->Cell($w[8], 7, ($row['pc']!='')?$row['pc'].' '.$unit[$row['pc_unit']]:'', 1, 0, 'R');
            $this->Cell($w[9], 7, ($row['pwh']!='')?$row['pwh'].' '.$unit[$row['pwh_unit']]:'', 1, 0, 'R');
            $this->Cell($w[10], 7, ($row['pfl']!='')?$row['pfl'].' '.$unit[$row['pfl_unit']]:'', 1, 0, 'R');
            $this->Cell($w[11], 7, ($row['psep']!='')?$row['psep'].' '.$unit[$row['psep_unit']]:'', 1, 0, 'R');
            $this->Cell($w[12], 7, ($row['temperatur']!='')?(($row['temperatur_unit']==2)?(32+9*$row['temperatur']/5):$row['temperatur']):'', 1, 0, 'R');
            $this->Cell($w[13], 7, $row['ket'], 1, 0, 'L');
            $this->Ln();
        }
        // Closing line
        $this->Cell(array_sum($w), 0, '', 'T');
        
        header('Set-Cookie: fileDownload=true; path=/');
        $this->Output('Daily Data Report '.$data['well']['well_name'].'.pdf','D');
    }

    // Page header
    function Header() {
        // Logo
        $this->Image('./assets/img/logo.jpg', null, null, 8);
        // Arial bold 15
        $this->SetFont('Times', 'B', 10);
        // Company name
        $this->Cell(15);
        $this->Cell(30, -8, 'Intermega Sabaku, Pte Ltd', 0, 0, 'C');
        // Line break
        $this->Ln(7);
    }

// Page footer
    function Footer() {
        // Position at 1.5 cm from bottom
        $this->SetY(-15);
        // Arial italic 8
        $this->SetFont('Times', '', 8);
        // Page number
        $this->Cell(0, 10, $this->PageNo(), 0, 0, 'C');
    }

    function DailyTable($header, $data) {
        
    }

}
