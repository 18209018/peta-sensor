<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Template{
    
    var $_ci;
    var $display;
    function __construct() {
        $this->_ci = &get_instance();
    }
    
    function display($view,$data=''){
        $this->_default($view,$data);
        $this->display['navbar'] = $this->_ci->load->view('template/navbar',$data,true);
        $this->display['sidebar'] = $this->_ci->load->view('template/sidebar',$data,true);
        $this->_ci->load->view('template/main',$this->display);
    }
    
    function display_sidebarless($view,$data=''){
        $this->_default($view,$data);
        $this->_ci->load->view('template/main-without-sidebar',$this->display);
    }
    
    function _default($view,$data=''){
        $this->display['header'] = $this->_ci->load->view('template/header',$data,true);
        $this->display['script'] = $this->_ci->load->view('template/script',$data,true);
        $this->display['navbar'] = $this->_ci->load->view('template/navbar-without-sidebar',$data,true);
        $this->display['content'] = $this->_ci->load->view($view,$data,true);
        $this->display['footer'] = $this->_ci->load->view('template/footer',$data,true);
    }
}